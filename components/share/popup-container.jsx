'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class SharePopupContainer extends ReactBase {
    stopPropagation(e) {
        e.stopPropagation();
    }
    render() {
        return (
            <div className="share-popup-container" onClick={this.props.hide}>
                <div className="content" onClick={this.stopPropagation}>
                    {this.props.children}
                </div>
            </div>
            );
    }
}

module.exports = SharePopupContainer;
