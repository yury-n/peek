'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class FacebookIcon extends ReactBase {
    isDisabled() {
        return !this.props.url;
    }
    handleClick() {
        if (!this.isDisabled()) {
            let url = 'http://www.facebook.com/sharer.php?';
            url += 'u=' + encodeURIComponent(this.props.url);

            const title = this.props.title + ' by ' + this.props.author.username + ' on DeviantArt';
            url += '&t=' + encodeURIComponent(title);
            window.open(url, 'sharer', 'toolbar=0,status=0,width=626,height=436');
        }

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'facebook',
            section: 'right_column',
            component: 'share_options',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        return (
            <div className="share-icon-wrap">
                <a href="javascript:void(0)" onClick={this.handleClick} className={classnames([
                    'share-icon',
                    'share-facebook',
                    {'disabled': this.isDisabled()}
                ])}>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="300px" height="300px" viewBox="0 0 300 300" enable-background="new 0 0 300 300">
                        <g>
                            <path id="f_3_" fill="#fff" d="M163.709,237.449v-79.992h26.85l4.02-31.174h-30.87V106.38c0-9.026,2.506-15.177,15.45-15.177
            l16.508-0.008V63.313c-2.856-0.379-12.655-1.228-24.055-1.228c-23.801,0-40.095,14.528-40.095,41.208v22.99h-26.919v31.174h26.919
            v79.992H163.709z"></path>
                        </g>
                    </svg>
                </a>
            </div>
            );
    }
}

module.exports = FacebookIcon;
