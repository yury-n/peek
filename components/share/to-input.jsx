'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Hotkeys = require('lib/da/hotkeys');

const AutocompleteSuggestion = require('lib/da/peek/components/share/autocomplete-suggestion');

class ToInput extends ReactBase {
    constructor(props) {
        super(props);

        this.state = {
            showAutocomplete: false,
            autocompleteSuggestions: [],
            highlightedIndex: -1,
            highlightedValue: false
        }
    }
    componentWillReceiveProps(nextProps) {
        // pick out autocomplete suggestions if we have our watchees and a value
        // prop is set
        if (this.props.watchees && nextProps.value.length > 2) {
            const best = [];
            const acceptable = [];
            // watchees is always sorted alphabetically on username
            this.props.watchees.forEach((watchee) => {
                const i = watchee.username.toLowerCase().indexOf(nextProps.value.toLowerCase());
                if (i === 0) {
                    // username starts with the letters the user has already
                    // typed, so this is a great suggestion
                    best.push(watchee);
                    return;
                }
                if (i > -1) {
                    // username has the letters in there somewhere, put it in
                    // acceptable array and we'll tack it on the end
                    acceptable.push(watchee);
                }
            });
            this.setState({
                autocompleteSuggestions: best.concat(acceptable).slice(0, 5),
                // if the user had previously used the keyboard to highlight a
                // suggestion unhighlight that as the list is changing
                highlightedIndex: -1,
                highlightedValue: false
            });
        } else {
            this.setState({
                autocompleteSuggestions: [],
                // if the user had previously used the keyboard to highlight a
                // suggestion unhighlight that as the list is changing
                highlightedIndex: -1,
                highlightedValue: false
            });
        }
    }
    upHandler(e) {
        e.preventDefault();
        let idx = this.state.highlightedIndex;
        // only move up if we haven't already moved up out of the list
        if (idx > -1) {
            idx--;
            this.highlightIndex(idx);
        }
    }
    downHandler(e) {
        e.preventDefault();
        let idx = this.state.highlightedIndex;
        // only move down if there are more options below
        if (idx < this.state.autocompleteSuggestions.length - 1) {
            idx++;
            this.highlightIndex(idx);
        }
    }
    enterHandler(e) {
        e.preventDefault();
        let idx = this.state.highlightedIndex;
        if (this.state.highlightedValue) {
            // update the input with the highlighted value
            this.props.setToValue(this.state.highlightedValue);
            // hide the suggestions list
            this.setState({
                showAutocomplete: false
            });
            // make highlightedIndex -1 so that the various states
            // update appropriately in highlightIndex
            idx = -1;
            this.highlightIndex(idx);
        }
    }
    backspaceHandler() {
        // when the user selects an item from the list using the enter key we
        // hide the list, if they backspace ensure the list can reappear
        if (!this.state.showAutocomplete) {
            this.setState({
                showAutocomplete: true
            });
        }
    }
    highlightIndex(idx) {
        // verify index has changed
        if (idx !== this.state.highlightedIndex) {
            this.setState({
                highlightedIndex: idx
            });
            if (this.state.autocompleteSuggestions[idx]) {
                this.setState({
                    highlightedValue: this.state.autocompleteSuggestions[idx].username
                });
            } else {
                this.setState({
                    highlightedValue: false
                });
            }
        }
    }
    registerHotkeys() {
        Hotkeys.register(['UP'], this.upHandler, true);
        Hotkeys.register(['DOWN'], this.downHandler, true);
        Hotkeys.register(['ENTER'], this.enterHandler, true);
        Hotkeys.register(['BACKSPACE'], this.backspaceHandler, true);
    }
    unregisterHotkeys() {
        Hotkeys.unregister(['UP'], this.upHandler, true);
        Hotkeys.unregister(['DOWN'], this.downHandler, true);
        Hotkeys.unregister(['ENTER'], this.enterHandler, true);
        Hotkeys.unregister(['BACKSPACE'], this.backspaceHandler, true);
    }
    showAutocomplete() {
        this.setState({
            showAutocomplete: true
        });
        this.registerHotkeys();
    }
    hideAutocomplete() {
        // lengthy setTimeout required or else blur updates state before click
        // handler executes...
        setTimeout(() => {
            this.setState({
                showAutocomplete: false,
                highlightedIndex: -1,
                highlightedValue: false
            });
        }, 250);
        this.unregisterHotkeys();
    }
    getAutocompleteList() {
        if (this.state.showAutocomplete && this.state.autocompleteSuggestions.length) {
            return (
                <ul className="autocomplete-list">
                    {this.state.autocompleteSuggestions.map((suggestion, index) => {
                        return (
                            <AutocompleteSuggestion
                                key={suggestion.userid}
                                setToValue={this.props.setToValue}
                                username={suggestion.username}
                                usericon={suggestion.usericon}
                                highlighted={index === this.state.highlightedIndex} />
                            );
                    })}
                </ul>
                );
        }
    }
    render() {
        return (
            <div className="autocomplete-wrap">
                <input
                    {...this.props}
                    type="text"
                    placeholder="To"
                    onFocus={this.showAutocomplete}
                    onBlur={this.hideAutocomplete} />
                {this.getAutocompleteList()}
            </div>
            );
    }
}

module.exports = ToInput;
