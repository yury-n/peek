'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const Hotkeys = require('lib/da/hotkeys');
const dapx = require('lib/da/dapx');

class FaveButton extends ReactBase {
    constructor() {
        super();
        Hotkeys.register(['f'], this.faveClick);
    }
    
    faveClick() {
        if (this.props.position === 0) {
            this.props.faveClick();

            // log event T26830
            dapx.sendStandardizedPayload({
                view: 'peek',
                element: 'fave_btn',
                section: 'right_column',
                component: 'buttons_block',
                action: 'click'
            }, this.props.getDapxExtraData());
        }
    }

    render() {
        let text = this.props.isFavourited ? 'faved' : 'fave';
        if (this.props.isFaving) {
            text = (
                <div className="dots-wrap">
                    <div className="dot dot-1" />
                    <div className="dot dot-2" />
                    <div className="dot dot-3" />
                </div>
            );
        }
        
        return (
            <div className="fave-button-wrap">
                <div className={classnames([
                    'fave-button',
                    'btn',
                    'dark',
                    'good',
                    {'faved': this.props.isFavourited}
                ])} title="Favourite"
                 onClick={ this.faveClick }>
                    <span className={classnames([
                        'fave-button-icon',
                        {
                            'icon-fave-open': !this.props.isFavourited,
                            'icon-fave-fill': this.props.isFavourited
                        }
                    ])} />
                    <span className="fave-button-text" data-sigil="peek-fave-button" >
                        { text }
                    </span>
                </div>
                <div className={classnames(["fave-button-arrow", {active: this.props.isFaveDropdownActive, faved: this.props.isFavourited}])} data-sigil="peek-more-button" onClick={ this.props.faveDropdownClick } title="Collections">
                    <span className="icon-arrow-down" />
                </div>
            </div>
        );
    }
}

module.exports = FaveButton;
