'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const dapx = require('lib/da/dapx');

class MetropolisBanner extends ReactBase {
    trackClick() { 
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'metropolis_btn',
            section: 'left_column',
            component: 'peek_banner',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    render() {
        return (
            <div className="campaign metropolis-campaign">
                <i className="icon"></i>
                <div className="title">
                    <h1>Capturing Change</h1>
                    <h2>Presented by Sony</h2>
                </div>
                <div className="btnWrapper">
                    <a href="https://alphauniverse.com" className="campaignBTN" onClick={ this.trackClick } target="_blank">EXPLORE ALPHA UNIVERSE</a>
                </div>
            </div>
       );
    }
}

module.exports = MetropolisBanner;
