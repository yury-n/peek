'use strict'

const Types = [
    'image',
    'freeform',
    'flash',
    'madefire'
];

module.exports = {
    types: Types,
    test: function(type) {
        return (Types.indexOf(type) !== -1);
    }
};
