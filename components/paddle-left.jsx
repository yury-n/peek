'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class PaddleLeft extends ReactBase {
    render() {
        const classes = ['peek-direction', 'peek-left'];

        if (this.props.visible) {
            classes.push('visible');
        } 

        return (<div data-sigil="peek-noclose" className={classes.join(' ')} onClick={this.props.slideLeft} />)
    }
}

module.exports = PaddleLeft;