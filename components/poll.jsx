'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const UserIcon = require('lib/da/user/components/user-icon');
const UserLink = require('lib/da/user/components/user-link');
const dateFormatter = require('lib/da/date-formatter');
const classnames = require('lib/tp/classnames/classnames');

class PollBody extends ReactBase {
    construct() {
        this.selectedAnswer = null;
    }

    selectAnswer(id) {
        this.selectedAnswer = id;
    }

    vote() {
        if (this.selectedAnswer === null || this.props.isVoting) {
            return;
        }

        this.props.actions.vote(this.props.poll.pollid.id, this.props.poll.pollid.splitid, this.selectedAnswer);
    }

    generateQuestion() {
        return <div className="poll-question" dangerouslySetInnerHTML={{ __html: this.props.poll.question }} />;
    }

    generatePercentFiller(inside, percent) {
        if ((!inside && percent > 10) || (inside && percent <= 10)) {
            return false;
        }

        return (<span className="percent-text">{ percent }%</span>);
    }

    generateBody() {
        if (this.props.poll.has_voted) {
            return (
                <div className="poll-answered">
                    { this.generateQuestion() }
                    { this.props.poll.answers.map((answer, idx) => {
                        const style = {
                            width: answer.percentvotes + '%'
                        };

                        return (
                            <div className="poll-answer" key={ idx }>
                                <div className="percent-bar">
                                    <div className={ classnames('percent-filler', {'has-votes': answer.percentvotes > 0}) } style={ style }>
                                        { this.generatePercentFiller(true, answer.percentvotes ) }
                                    </div>
                                    { this.generatePercentFiller(false, answer.percentvotes ) }
                                </div>

                                <span>{ answer.votes } deviants said </span>

                                <strong className="answer-text" dangerouslySetInnerHTML={{ __html: answer.answer }} />
                            </div>
                        );
                    }) }
                </div>);
        }

        return (
            <div>
                { this.generateQuestion() }

                { this.props.poll.answers.map((answer, idx) => {
                    const name = "poll-" + this.props.poll.pollid.pollid;

                    return (
                        <div className="poll-answer" key={ idx }>
                            <label onClick={ this.selectAnswer.bind(this, answer.answerid) }>
                                <input type="radio" name={ name } disabled={ this.props.isVoting }/>
                                <span dangerouslySetInnerHTML={{ __html: answer.answer }} />
                            </label>
                        </div>
                    );
                }) }
            </div>
        );
    }

    generateVoteButton() {
        if (this.props.poll.has_voted) {
            return false;
        }

        return (
            <div className="vote-box light">
                <span className={ classnames('btn', 'primary', {disabled: this.props.isVoting}) } onClick={ this.vote }>{ this.props.isVoting ? 'voting' : 'vote' }</span>
                <span className="vote-count">{ this.props.poll.votes > 0 ? this.props.poll.votes : 'No' } deviants have voted</span>
            </div>
        );
    }

    render() {
        if (this.props.loading) {
            return null;
        }

        return (
            <div className="poll no-skin">
                <div className="poll-top">
                    <UserIcon className="poll-usericon" username={this.props.nodeMetadata.author.username} usericon={this.props.nodeMetadata.author.usericon} />
                    <UserLink className="poll-userlink" username={this.props.nodeMetadata.author.username} />

                    <div className="poll-date">
                        { dateFormatter.mdy(dateFormatter.parse(this.props.publishedTime)) }
                    </div>
                </div>

                { this.generateBody() }

                { this.generateVoteButton() }
            </div>
        );
    }
}

class Poll extends ReactBase {
    render() {
        if (!this.props.contentLoaded) {
            return (<PollBody loading={true} />);
        }

        return (<PollBody {...this.props} loading={false} />);
    }
}

module.exports = Poll;

