'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class CloseX extends ReactBase {
    render() {
        return (
            <a {...this.props} href="javascript:void(0)" className="close" dangerouslySetInnerHTML={{__html: '&times;'}} />
            );
    }
}

module.exports = CloseX;
