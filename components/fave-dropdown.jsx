'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const ScrollStopper = require('lib/da/components/scroll-stopper');
const Popup = require('lib/da/popup');
const FaveNewCollection = require('lib/da/peek/components/fave-new-collection');
const FaveDropdownItem = require('lib/da/peek/components/fave-dropdown-item');
const dapx = require('lib/da/dapx');

class FaveDropdown extends ReactBase {   

    handleNew() {
        Popup.create(FaveNewCollection, { props: this.props }).show();

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'add_collection',
            section: 'right_column',
            component: 'collection_dropdown',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    generateCollections() {
        let collections = [];
        for (let i = 0; i < this.props.collections.length; i++) {

            const collection = this.props.collections[i];
            let subtitle = false;
            if (collection.name === 'Featured') {
                subtitle = <span className="desc">Your default faves collection</span>;
            }
            collections.push(
                <FaveDropdownItem
                    key={collection.folderid}
                    folderid={collection.folderid}
                    name={collection.name}
                    icon={collection.icon}
                    subtitle={subtitle}
                    faveDropdownSelect={this.props.faveDropdownSelect}
                    getDapxExtraData={ this.props.getDapxExtraData }
                />
            );
        }

        return collections;
    }

    generateNewCollection() {
        if (!this.props.collectionsLoaded) {
            return false;
        }

        return (
            <li key="0">
                <a href="javascript:void(0)" onClick={this.handleNew}><i className="add"></i><strong>Create a new Collection...</strong></a>
            </li>
        );
    }

    render() {
        return (
            <div className={ classnames(['fave-dropdown'], {'active': this.props.isFaveDropdownActive}) }>
                <h2>Select a faves collection:</h2>
                <ul className="buttons" ref="buttons">
                    <ScrollStopper>
                        <ul className="buttons" ref="buttons">
                            { this.props.collectionsLoaded ? false : <li className="loading">Loading...</li>}
                            <span className="overflow" data-sigil="scroll-stopper-scrollable">{ this.generateCollections() }</span>
                            { this.generateNewCollection() }
                        </ul>
                    </ScrollStopper>
                </ul>
            </div>
       );
    }
}

module.exports = FaveDropdown;
