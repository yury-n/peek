'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');

class AutocompleteSuggestion extends ReactBase {
    handleClick() {
        this.props.setToValue(this.props.username);
    }
    render() {
        return (
            <li onClick={this.handleClick} className={classnames({'highlighted': this.props.highlighted})}>
                <img src={this.props.usericon.replace('http:', '')} />
                {this.props.username}
            </li>
            );
    }
}

module.exports = AutocompleteSuggestion;
