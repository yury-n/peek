'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');

class TabLink extends ReactBase {
    handleClick() {
        this.props.changeActiveTab(this.props.name);
    }
    render() {
        return (
            <a className={classnames([
                'tab-link',
                {'active': this.props.isActive}
            ])} href="javascript:void(0)" onClick={this.handleClick}>{this.props.name}</a>
        );
    }
}

module.exports = TabLink;
