'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const dapx = require('lib/da/dapx');

class NoteIcon extends ReactBase {
    handleClick() {
        this.props.openNoteModal();

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'note',
            section: 'right_column',
            component: 'share_options',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        return (
            <div className="share-icon-wrap">
                <a href="javascript:void(0)" onClick={this.handleClick} className="share-icon share-note">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="320px" height="320px" viewBox="0 0 320 320" enable-background="new 0 0 320 320">
                        <g fill="#05CC47">
                            <polygon points="163.216,176.759 161.862,178.256 161.723,178.13 161.586,178.256 160.232,176.781 83.027,106.933 72.276,117.684
                65.532,124.428 65.532,133.965 65.532,228.315 113.023,228.315 162.701,228.315 206.972,228.315 216.509,228.315 223.253,221.571
                247.621,197.203 254.365,190.459 254.365,180.923 254.365,93.032  "></polygon>
                            <polygon points="161.686,168.532 250.783,86.691 112.806,86.691 103.269,86.691 96.525,93.435 88.048,101.912  "></polygon>
                        </g>
                    </svg>
                </a>
            </div>
            );
    }
}

module.exports = NoteIcon;
