'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Alt = require('lib/tp/alt');
const SharePopupActions = require('lib/da/peek/actions/share-popup');
const NotePopupActions = require('lib/da/peek/actions/note-popup');
const NotePopupStore = require('lib/da/peek/stores/note-popup');
const Loading = require('lib/da/peek/components/loading');

const SharePopupContainer = require('lib/da/peek/components/share/popup-container');
const ShareInfo = require('lib/da/peek/components/share/info');
const ToInput = require('lib/da/peek/components/share/to-input');
const CloseX = require('lib/da/peek/components/share/close-x');

class NotePopup extends ReactBase {
    constructor(props) {
        super(props);
        this.alt = new Alt();
        this.alt.addActions('SharePopupActions', SharePopupActions);
        this.alt.addActions('NotePopupActions', NotePopupActions);
        this.alt.addStore('NotePopupStore', NotePopupStore);
        this.sharePopupActions = this.alt.getActions('SharePopupActions');
        this.notePopupActions = this.alt.getActions('NotePopupActions');
        this.notePopupStore = this.alt.getStore('NotePopupStore');

        this.state = this.notePopupStore.getState();
    }
    componentDidMount() {
        this.notePopupStore.listen(this.onChange);

        this.notePopupActions.fetchWatchees();

        switch (this.props.type) {
            case 'status':
                this.sharePopupActions.fetchStatusContent(this.props.id, this.props.author.userid);
                break;
            case 'collection':
                this.sharePopupActions.setCollectionData({author: this.props.author, collection: this.props.collection});
                break;
            case 'poll':
                this.notePopupActions.setPollData({author: this.props.author, id: this.props.id});
                break;
            default:
                this.sharePopupActions.fetchDeviationMetadata(this.props.id);
                break;
        }
    }
    componentWillUnmount() {
        this.notePopupStore.unlisten(this.onChange);
    }
    onChange() {  
        this.setState(this.notePopupStore.getState());
    }
    // callback for "To" input onChange event
    handleToChange(e) {
        this.notePopupActions.setToValue(e.target.value);
    }
    // callback for "Message" textarea onChange event
    handleMessageChange(e) {
        this.notePopupActions.setMessageValue(e.target.value);
    }
    // reset the form then close the share popup
    clearAndClose() {
        this.sharePopupActions.resetForm();
        this.props.hide();
    }
    // Send button is disabled until we have enough data to try and send. If
    // sharing a deviation prepend the thumb to the message. If sharing a status
    // prepend the status URL.
    sendNote() {
        const body = this.state.sharedItem + '\n\n' + this.state.message;
        this.notePopupActions.sendNote(this.state.to, this.state.subject, body);
    }
    // display correct sub heading based on the item being shared
    getSubHeadingText() {
        switch (this.props.type) {
            case 'status':
                return 'Share this status with another deviant';
            case 'collection':
                return 'Share this collection with another deviant';
            default:
                return 'Share this deviation with another deviant';
        }
    }
    // renders basic info about the item being shared
    getInfo() {
        if (this.state.loaded) {
            return (
                <ShareInfo
                    thumbSrc={this.props.thumbSrc}
                    username={this.state.author.username}
                    usericon={this.state.author.usericon}
                    mature={this.props.mature}
                    title={this.state.title} />
                );
        }
    }
    // renders the feedback message in state if set, otherwise renders the
    // cancel/submit buttons
    getButtonsOrFeedback() {
        if (!this.state.loaded) {
            return <Loading />;
        }

        if (this.state.feedback) {
            let reset = null;
            if (this.state.showResetWithFeedback) {
                reset = <a href="javascript:void(0)" onClick={this.sharePopupActions.resetForm}>Reset Form</a>;
            }
            return (
                <p className="feedback">
                    {this.state.feedback + ' '}
                    {reset}
                </p>
                );
        } else {
            return (
                <div className="buttons-wrap">
                    <button className="btn cancel" onClick={this.clearAndClose}>Cancel</button>
                    <button className="btn primary submit" onClick={this.sendNote} disabled={!this.state.to}>Send</button>
                </div>
                );
        }
    }
    render() {
        return (
            <SharePopupContainer hide={this.clearAndClose}>
                <div className="heading">Send A Note</div>
                <div className="sub-heading">{this.getSubHeadingText()}</div>
                {this.getInfo()}
                <ToInput
                    value={this.state.to}
                    onChange={this.handleToChange}
                    setToValue={this.notePopupActions.setToValue}
                    watchees={this.state.watchees} />
                <textarea ref="message" placeholder="Your message..." value={this.state.message} onChange={this.handleMessageChange} />
                {this.getButtonsOrFeedback()}
                <CloseX onClick={this.clearAndClose} />
            </SharePopupContainer>
            );
    }
}

module.exports = NotePopup;
