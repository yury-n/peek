'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class PaddleLoading extends ReactBase {
    render() {
        const classes = ['peek-direction', 'spinner'];

        if (this.props.visible) {
            classes.push('visible');
        }

        return (<div data-sigil="peek-noclose" className={classes.join(' ')}>
                      <div className="spinner-container container1">
                        <div className="circle1"></div>
                        <div className="circle2"></div>
                        <div className="circle3"></div>
                        <div className="circle4"></div>
                      </div>
                      <div className="spinner-container container2">
                        <div className="circle1"></div>
                        <div className="circle2"></div>
                        <div className="circle3"></div>
                        <div className="circle4"></div>
                      </div>
                      <div className="spinner-container container3">
                        <div className="circle1"></div>
                        <div className="circle2"></div>
                        <div className="circle3"></div>
                        <div className="circle4"></div>
                      </div>
                </div>)
    }
}

module.exports = PaddleLoading;