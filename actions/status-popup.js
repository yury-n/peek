'use strict';

const dapi = require('lib/da/dapi');

class StatusPopupActions {
    constructor() {
        this.generateActions('setStatusValue', 'postStatusDone');
    }
    postStatus(attachmentType, sharedItemId, parentId, status) {
        this.dispatch();

        dapi.request('/status/post', {
            attachment_type: attachmentType,
            id: sharedItemId,
            parentid: parentId,
            body: status
        }).then((data) => {
            if (data.statusid) {
                // we received a status id so we know it was successful
                this.actions.postStatusDone('Status posted successfully.');
            } else {
                // server didn't return error status but we didn't get the
                // expected response either ¯\_(ツ)_/¯ this probably won't happen
                this.actions.postStatusDone('Something probably failed. ¯\_(ツ)_/¯');
            }
        }, (error) => {
            if (error.error_description) {
                // server returned an error description so show that
                this.actions.postStatusDone('Error: ' + error.error_description);
            } else {
                // no error description so display generic error
                this.actions.postStatusDone('Error: Server returned an error.');
            }
        });
        dapi.send();
    }
}

module.exports = StatusPopupActions;
