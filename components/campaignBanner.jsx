'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

// Include banners for campaigns here.
const Metropolis = require('lib/da/peek/components/campaigns/metropolis');

class CampaignBanner extends ReactBase {
    getCampaign() { 
        switch (this.props.campaignKey) {
            // T26887
            case 'SONY_METROPOLIS_2015':
                return (<Metropolis getDapxExtraData={ this.props.getDapxExtraData } />);
        }
    }
    render() {
        return (<span className="campaignBanner">
                { this.getCampaign() }
                </span>
       );
    }
}

module.exports = CampaignBanner;
