'use strict';

const GallectionTypes = require('lib/da/peek/constants/gallection-types');
const dateFormatter = require('lib/da/date-formatter');
const update = require('lib/tp/react').addons.update;

const toCamel = (str) => {
    return str.replace(/(_[a-z])/g, ($1) => {
        return $1.toUpperCase().replace('_','');
    });
}

// store collections globally
let collectionsData = {
    loaded: false,
    collections: []
}

class Store {
    constructor() {
        this.bindActions(this.alt.getActions('SlideActions')); 

        // are things loaded yet
        this.contentLoaded = false;
        this.collectionGridLoaded = false;
        this.mfaLoaded = false;
        this.mltLoaded = false;

        // collections
        this.collectionsData = collectionsData;

        // open or closed states
        this.zoomed = false; // Deviation "full view" state
        this.commentViewActive = false; // view all comments state
        this.allowStateUpdates = true; // if we've unmounted, don't allow state updates
        this.shareOpen = false; // the share box is active
        this.descriptionCollapsed = true; // there is 'more' in the description
        this.tagsCollapsed = true; // there is 'more' in the tags
        this.expanded = false;

        // data is in transit
        this.isCommenting = false;
        this.isFaving = false;
        this.isFaveDropdownActive = false;
        this.isSaving = false;
        this.isVoting = false;

        // initial state set by onFetchContentDone
        this.isFavourited = null;
        this.isSaved = null;

        this.newCollection = this.newCollectionDefaults();

        const defaults = {
            page: 0,
            title: '',
            truncatedDescription: '',
            url: 'javascript:void(0)',
            tags: [],
            categoryPathTitles: [],
            categoryPath: [],
            relatedArt: {
                mfa: 'Loading...',
                mlt: 'Loading...'
            },
            comments: {
                thread: []
            },
            stats: {
                comments: 0,
                views: 0,
                favourites: 0,
            },
            freeformContent: '',
            status: {},
            poll: {},
            deviations: [],
            activeTab: 'info',
            publishedTime: 0
        }

        // set all non-boolean defaults
        const keys = Object.keys(defaults);
        for (let i = 0; i < keys.length; i++) {
            this[keys[i]] = defaults[keys[i]];
        }
    }

    newCollectionDefaults() {
        return {
            name: '',
            description: '',
            tags: '',
            antisocial: true
        }
    }

    onFetchContentDone(data) {
        if ('metadata' in data) {
            const keys = Object.keys(data.metadata.metadata[0]);
            for (let i = 0; i < keys.length; i++) {
                let thisParam = toCamel(keys[i]);
                this[thisParam] = data.metadata.metadata[0][keys[i]];
            }
        }

        // we need to "massage" the data we get from statuses
        if ('status' in data) {
            this.title = 'View Status';
            this.url = data.status.url;
            this.stats.comments = data.status.comments_count;
            this.publishedTime = data.status.ts;

            this.status = data.status;
        }

        if ('collection' in data) {
            this.collection = data.collection;
            this.stats.comments = data.comments.total;
            this.title = this.collection.collection_data.gallection_meta.name;
            let urlPart = '';
            switch (this.collection.collection_data.gallection_meta.typeid) {
                case GallectionTypes.GALLERY:
                    urlPart = 'gallery';
                    break;

                case GallectionTypes.COLLECTION:
                    urlPart = 'favourites';
                    break;
            }

            const baseUrl = 'http://' + this.collection.curator.username + '.deviantart.com/' + urlPart + '/';
            if (this.title == 'Featured') {
                this.url = baseUrl;
            } else {
                this.url = baseUrl + this.collection.collection_data.gallection_meta.gallectionid;
            }

            this.publishedTime = false; // gallections don't have a published time
        }

        if ('poll' in data) {
            this.poll = data.poll;
            this.stats.comments = data.comments.total;
            this.title = 'View Poll';

            this.url = 'http://' + data.poll.user.username + '.deviantart.com/journal/poll/' + data.poll.pollid.id;
            this.publishedTime = data.poll.ts;
        }

        this.freeformContent = data.freeform || '';
        this.comments = data.comments || false;

        if (this.type == 'status' && this.status.is_deleted) {
            this.ownerid = 0;
        } else {
            this.ownerid = (this.author || {}).userid || 0;
        }

        this.contentLoaded = true;

        return this.allowStateUpdates;
    }

    onFetchStatusAttachmentDone(data) {
        this.status = update(this.status, {$merge: {attachment: data}});
        return this.allowStateUpdates;
    }

    onFetchRelatedArtDone(relatedArt) {
        this.mfaLoaded = true;
        this.mltLoaded = true;
        this.relatedArt = relatedArt;
        return this.allowStateUpdates;
    }

    onFetchCollectionGridDone(response) {
        this.deviations = response.deviations; 
        this.collectionGridLoaded = true;
        return this.allowStateUpdates;
    }

    onSlideMount() {
        this.allowStateUpdates = true;
    }

    onSlideUnmount() {
        this.allowStateUpdates = false;
    }
    
    onDeviationImageClick() {
        this.zoomed = !this.zoomed;
    }

    onMoreCommentsClick() {
        this.commentViewActive = !this.commentViewActive;
    }

    onPaginateClick(offset) {
        this.commentLoading = true;
        this.page = offset / 10;
    }

    onPaginateClickDone(data) {
        this.comments = data;
        this.commentLoading = false;
    }

    onSetActiveInfoTab(tabname) {
        this.activeTab = tabname;
    }

    onWatchUser() {
        this.author = update(this.author, {is_watching: {$set: true}});
        return this.allowStateUpdates;
    }

    onUnwatchUser() {
        this.author = update(this.author, {is_watching: {$set: false}});
        return this.allowStateUpdates;
    }

    onWatchChangeDone(result) {
        if (!result.success) {
            this.author = update(this.author, {is_watching: {$set: !this.author.is_watching}});
            return this.allowStateUpdates;
        }
    }
    onFaveClick() {
        this.isFaving = true;
    }
    onFaveDropdownClick() {
        this.isFaveDropdownActive = !this.isFaveDropdownActive;
        // ensure only one of these is open
        if (this.shareOpen) {
            this.shareOpen = false;
        }
    }
    onFaveDropdownFetchDone(collections) {
        collectionsData.loaded = true;
        collectionsData.collections = collections;
        return this.allowStateUpdates;
    }

    onFaveDropdownSelect(payload) {
        this.isFaveDropdownActive = false;
        this.isFavourited = true;
        this.isFaving = true;
        // set preview image
        if (payload) {
            for (let i = 0; i < this.collectionsData.collections.length; i++) {
                let collection = this.collectionsData.collections[i];
                if (collection.folderid == payload.folderid) {
                    this.collectionsData.collections[i].icon = payload.icon;
                }
            }
        }

        return this.allowStateUpdates;
    }

    onFaveDropdownSelectDone() {
        this.isFaving = false;
    }

    onFaveNewCollectionSubmit(payload) {
        this.collectionsData.collections.push({
            name : "Creating " + payload.name + "...", folderid: 0
        });
        return this.allowStateUpdates;
    }

    onFaveNewCollectionSubmitDone(payload) {
        this.collectionsData.collections.pop();
        this.collectionsData.collections.push({
            name : payload.name, folderid: payload.folderid
        });
        this.newCollection = this.newCollectionDefaults();
        return this.allowStateUpdates;
    }

    onFaveNewCollectionError() {
        this.collectionsData.collections.pop();
        this.newCollection = this.newCollectionDefaults();
        return this.allowStateUpdates;
    }

    onFaveNewCollectionChange(payload) {
        this.newCollection[payload.name] = payload.value;
    }

    onFaveClickDone() {
        this.isFaving = false;
        this.isFavourited = !this.isFavourited
        // if the item were in save it would have been removed, so reflect that
        // here
        this.isSaved = false;
        return this.allowStateUpdates;
    }

    onSaveDeviation() {
        this.isSaving = true;
    }

    onSaveDeviationDone() {
        this.isSaved = true;
        this.isSaving = false;
    }

    onUnsaveDeviation() {
        this.isSaving = true;
    }

    onUnsaveDeviationDone() {
        this.isSaved = false;
        this.isSaving = false;
    }

    onSaveError() {
        this.isSaving = false;
    }

    onPostComment() {
        this.isCommenting = true;
    }

    onPostCommentDone(data) {
        this.stats = update(this.stats, {
            comments: {
                $set: this.stats.comments + 1
            }
        });
        this.isCommenting = false;

        // If we're posting a sub-comment we need to insert the comment inline
        if (data.parentid) {
            let index = 0;
            for (let i = 0; i < this.comments.thread.length; i++) {
                // if the comment id from the thread is the parent id of the new comment we have the id to insert after.
                //i+1 is the index to insert at, after the parent comment.
                if (this.comments.thread[i].commentid.id == data.parentid.id) {
                    index = i + 1;
                    break;
                }
            }

            // splice new comment into the thread at the correct point.
            this.comments = update(this.comments, {thread: {$splice: [[index, 0, data]]}});
        } else {
            // if we're adding a new root level comment just push it onto the
            // thread. It'll show up right after the comment box.
            this.comments = update(this.comments, {thread: {$unshift: [data]}});
        }

        // not likely that they've moved away yet, but just in case
        return this.allowStateUpdates;
    }

    onToggleShareOpen() {
        this.shareOpen = !this.shareOpen;
        // ensure only one of these is open
        if (this.isFaveDropdownActive) {
            this.isFaveDropdownActive = false;
        }
    }

    onToggleInfoDescriptionCollapsed() {
        this.descriptionCollapsed = !this.descriptionCollapsed;
    }

    onToggleInfoTagsCollapsed() {
        this.tagsCollapsed = !this.tagsCollapsed;
    }

    onToggleCollectionExpanded() {
        this.expanded = !this.expanded;
    }

    onVote() {
        this.isVoting = true;
    }

    onVoteDone(data) {
        // we need to update the poll because the percentages will have changed
        if ('poll' in data) {
            this.isVoting = false;
            this.poll = data.poll;
        }
    }

    onSetTrackingPixelHTML(html) {
        this.trackingPixelHTML = html;
    }

    onForceState(newState) {
        if (this.allowStateUpdates) {
            this.setState(newState);
        }
    }
}

module.exports = Store;
