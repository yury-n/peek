'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const popup = require('lib/da/popup');

const NotePopup = require('lib/da/peek/components/share/note-popup');
const StatusPopup = require('lib/da/peek/components/share/status-popup');

const StatusIcon = require('lib/da/peek/components/share/icons/status');
const NoteIcon = require('lib/da/peek/components/share/icons/note');
const FacebookIcon = require('lib/da/peek/components/share/icons/facebook');
const TwitterIcon = require('lib/da/peek/components/share/icons/twitter');
const TumblrIcon = require('lib/da/peek/components/share/icons/tumblr');
const PinterestIcon = require('lib/da/peek/components/share/icons/pinterest');
const RedditIcon = require('lib/da/peek/components/share/icons/reddit');

class Share extends ReactBase {
    componentWillUnmount() {
        if (this.notePopup) {
            this.notePopup.delete();
        }
        if (this.statusPopup) {
            this.statusPopup.delete();
        }
    }

    getThumbSrc() {
        // thumbSrc can only be determined by the metadata attached to the peek thumb
        if (this.props.sizing && this.props.sizing[0]) {
            return this.props.sizing[0].src;
        }
        return false;
    }

    openStatusModal() {
        if (!this.statusPopup) {
            this.statusPopup = popup.create(StatusPopup, {
                classes: ['fade'],
                animate: true,
                hideOnEscape: true,
                position: false,
                displayBackdrop: true,
                props: {
                    id: this.props.id,
                    thumbSrc: this.getThumbSrc(),
                    type: this.props.type,
                    author: this.props.author,
                    collection: this.props.collection,
                    hide: this.hidePopups,
                    mature: this.props.mature
                }
            });
        }
        window.requestAnimationFrame(this.statusPopup.show.bind(this.statusPopup));
    }

    openNoteModal() {
        if (!this.notePopup) {
            this.notePopup = popup.create(NotePopup, {
                classes: ['fade'],
                animate: true,
                hideOnEscape: true,
                position: false,
                displayBackdrop: true,
                props: {
                    id: this.props.id,
                    thumbSrc: this.getThumbSrc(),
                    type: this.props.type,
                    author: this.props.author,
                    collection: this.props.collection,
                    hide: this.hidePopups,
                    mature: this.props.mature
                }
            });
        }
        window.requestAnimationFrame(this.notePopup.show.bind(this.notePopup));
    }

    hidePopups() {
        if (this.notePopup) {
            this.notePopup.hide();
        }
        if (this.statusPopup) {
            this.statusPopup.hide();
        }
    }

    render() {
        let triangleStyle = {};
        if (this.props.triangleLeft) {
            triangleStyle.left = this.props.triangleLeft;
        }
        return (
            <div className={classnames([
                'share-container',
                {'open': this.props.isOpen}
            ])}>
                <div className="share-button-container">
                    <div className="triangle" style={triangleStyle} />
                    <div className="buttons">
                        <StatusIcon
                            openStatusModal={this.openStatusModal}
                            disabled={this.props.disableStatusShare}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                        <NoteIcon
                            openNoteModal={this.openNoteModal}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                        <FacebookIcon
                            url={this.props.url}
                            title={this.props.title}
                            author={this.props.author}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                        <TwitterIcon
                            author={this.props.author}
                            twitterHandle={null /* TODO? */}
                            title={this.props.title}
                            url={this.props.url}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                        <TumblrIcon
                            type={this.props.type}
                            src={this.props.src}
                            url={this.props.url}
                            title={this.props.title}
                            author={this.props.author}
                            description={this.props.description}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                        <PinterestIcon
                            url={this.props.url}
                            src={this.props.src}
                            description={this.props.description}
                            title={this.props.title}
                            author={this.props.author}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                        <RedditIcon
                            url={this.props.url}
                            getDapxExtraData={ this.props.getDapxExtraData }
                        />
                    </div>
                </div>
            </div>
            );
    }
}

module.exports = Share;
