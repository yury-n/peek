'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const UserIcon = require('lib/da/user/components/user-icon');
const UserLink = require('lib/da/user/components/user-link');
const formatter = require('lib/da/date-formatter');
const CommentBox = require('lib/da/peek/components/commentbox');
const classnames = require('lib/tp/classnames/classnames');

class Comment extends ReactBase {
    constructor(props) {
        super(props);

        this.state = {
            commentboxVisible: false
        };
    }

    toggleActive() {
        this.setState({ commentboxVisible : !this.state.commentboxVisible });
    }

    postComment(type, id, ownerid, body, parentid) {
        this.toggleActive();
        this.props.postComment(type, id, ownerid, body, parentid);
    }
    
    formatCommentTs(timestamp) {
        return formatter.miniTimeSince(formatter.parse(timestamp));
    }

    formatCommentBody() {
        let body = this.props.comment.body;

        // Hidden comments get a body of "[Hidden by Foo]", but we don't want the brackets
        if (this.props.comment.hidden) {
            body = body.match(/^\[(.*)\]$/)[1];
        }

        return body;
    }

    render() {
        const classes = ['comment'];
        const depthIndex = { 
            1 : 'first',
            2 : 'second',
            3 : 'third',
            4 : 'fourth',
            5 : 'fifth'
        };

        if (this.props.depth > 0) {
            classes.push(depthIndex[this.props.depth]);
        }
        if (this.props.comment.hidden) {
            classes.push('hidden');
        }

        return(<div className={ classnames(classes, {child: this.props.comment.parentid}) } >
                    <div className="icon">
                        <UserIcon username={ this.props.comment.user.username } usericon={ this.props.comment.user.usericon} />
                    </div>
                    <div className="comment-details">
                        <UserLink className="user-name" username={ this.props.comment.user.username } />
                        <a href={ "http://comments.deviantart.com/" + this.props.comment.commentid.typeid + "/" + this.props.comment.commentid.splitid + '/' + this.props.comment.commentid.id } className="timestamp">
                            { this.formatCommentTs(this.props.comment.posted) }
                        </a>
                    </div>
                    <div className="comment-content" dangerouslySetInnerHTML={{__html: this.formatCommentBody() }} />
                    <div className="comment-foot">
                        <a href="javascript:void(0)" className="reply" onClick={ this.toggleActive }>REPLY</a>
                        <a href="javascript:void(0)" className="report">REPORT SPAM</a>
                        <div className="commentContainer">
                            <CommentBox
                                toggleActive={ this.toggleActive }
                                isCommenting={ this.props.isCommenting }
                                parentid={ this.props.comment.commentid.id }
                                itemid={ this.props.itemid }
                                isVisible={ this.state.commentboxVisible }
                                id={ this.props.comment.commentid.id }
                                type={ this.props.type }
                                postComment={ this.postComment }
                                ownerid={ this.props.ownerid }
                                getDapxExtraData={ this.props.getDapxExtraData }
                                />
                        </div>
                    </div>
                </div>);
    }
}

module.exports = Comment;
