'use strict';

class NotePopupStore {
    constructor() {
        this.bindActions(this.alt.getActions('SharePopupActions'));
        this.bindActions(this.alt.getActions('NotePopupActions'));
        this.to = '';
        this.message = '';
        this.feedback = null;
        this.loaded = false;
        this.watchees = [];
        this.author = {};
    }
    onFetchStatusContent(data) {
        if (!data || data.is_deleted) {
            this.author = {};
            this.title = 'Status';
            this.sharedItem = '';
            this.feedback = 'Sorry, this status cannot be shared.';
        } else {
            this.author = data.author;
            this.title = this.author.username + '\'s Status';

            this.sharedItem = data.url;
        }

        this.subject = this.title;
        this.loaded = true;
    }
    onFetchDeviationMetadata(data) {
        if (!data) {
            this.title = 'Deviation';
            this.author = {};
            this.sharedItem = '';
            this.subject = '';
            this.feedback = 'Sorry, this deviation cannot be shared.';
        } else {
            this.title = data.metadata[0].title;
            this.author = data.metadata[0].author;

            this.sharedItem = ':thumb' + data.metadata[0].deviationid + ':';
            this.subject = this.title + ' by ' + this.author.username;
        }

        this.loaded = true;
    }
    onSetCollectionData(data) {
        this.author = data.author;
        this.title = data.collection.gallection_meta.name;
        this.subject = this.title + ' by ' + this.author.username;

        const baseUrl = 'http://' + this.author.username + '.deviantart.com/favourites/';

        // we can't directly link to featured even though it has a unique id
        if (this.title == 'Featured') {
            this.sharedItem = baseUrl;
        } else {
            this.sharedItem = baseUrl + data.collection.gallection_meta.gallectionid;
        }

        this.loaded = true;
    }
    onSetPollData(data) {
        this.author = data.author;
        this.title = data.author.username + '\'s Poll';
        this.subject = this.title;
        this.sharedItem = 'http://' + data.author.username + '.deviantart.com/journal/poll/' + data.id;
        this.loaded = true;
    }
    onSetToValue(value) {
        // in the event that a feedback message is showing, allow the user to
        // re-enable the send buttons by typing in the To input rather than
        // forcing them to reset the entire form
        if (this.feedback) {
            this.feedback = null;
        }
        this.to = value;
    }
    onSetMessageValue(value) {
        // in the event that a feedback message is showing, allow the user to
        // re-enable the send buttons by typing in the Message textarea rather
        // than forcing them to reset the entire form
        if (this.feedback) {
            this.feedback = null;
        }
        this.message = value;
    }
    onResetForm() {
        this.to = '';
        this.message = '';
        this.feedback = null;
    }
    onSendNote() {
        this.feedback = 'Sending...';
        this.showResetWithFeedback = false;
    }
    onSendNoteDone(feedback) {
        this.feedback = feedback;
        this.showResetWithFeedback = true;
    }
    onFetchWatchees(watchees) {
        this.watchees = watchees;
    }
}

module.exports = NotePopupStore;
