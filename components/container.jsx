'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Alt = require('lib/tp/alt');
const classnames = require('lib/tp/classnames/classnames');

const Actions = require('lib/da/peek/actions/container');
const Store = require('lib/da/peek/stores/container');

const PaddleLeft = require('lib/da/peek/components/paddle-left');
const PaddleRight = require('lib/da/peek/components/paddle-right');
const PaddleLoading = require('lib/da/peek/components/paddle-loading');
const Slide = require('lib/da/peek/components/slide');

class PeekContainer extends ReactBase {
    constructor(props) {
        super(props);

        this.alt = new Alt();
        this.alt.addActions('PeekContainerActions', Actions);
        this.alt.addStore('PeekContainerStore', Store);

        this.state = this.alt.getStore('PeekContainerStore').getState();
    }
    componentDidMount() {
        this.alt.getStore('PeekContainerStore').listen(this.onChange);
    }
    componentWillUnmount() {
        this.alt.getStore('PeekContainerStore').unlisten(this.onChange);
    }
    onChange() {
        this.setState(this.alt.getStore('PeekContainerStore').getState())
    }
    slideLeft() {
        this.alt.getActions('PeekContainerActions').slideLeft();
    }
    slideRight() {
        this.alt.getActions('PeekContainerActions').slideRight();
    }
    getSlideProps() {
        const slideProps = this.props.slideProps;
        if (slideProps.takeover) {
            const actions = this.alt.getActions('PeekContainerActions');
            slideProps.takeover.log = actions.logTakeoverClick.bind(actions);
        }

        return slideProps;
    }
    /**
     * Returns an array of objects represting up to 5 slides. The center slide
     * plus up to 2 on either side.
     *
     * @return {Array} Array of objects with data for the Slide components props
     */
    getSlides() {
        const centerSlide = this.state.slides[this.state.activeSlideId];
        const slides = [];
        if (centerSlide) {
            slides.push(centerSlide);

            // because this.state.slides is an object, we cannot rely on it
            // always being in the correct order (or that things are actually
            // sequential) so we have to traverse it like the linked list that
            // it is
            let rightSlide = this.state.slides[centerSlide.right];
            let leftSlide = this.state.slides[centerSlide.left];

            let rightCount = 0;
            while (rightSlide && rightCount < 2) {
                slides.push(rightSlide);
                rightSlide = this.state.slides[rightSlide.right];
                rightCount++;
            }

            let leftCount = 0;
            while (leftSlide && leftCount < 2) {
                slides.unshift(leftSlide);
                leftSlide = this.state.slides[leftSlide.left];
                leftCount++;
            }
        }
        return slides;
    }
    render() {
        const slides = this.getSlides();
        const slideProps = this.getSlideProps();

        return (
            <div data-sigil="peek-container" className={classnames([
                'peek-container',
                {
                    'slideable': this.state.slideable, 
                    'taken-over': this.props.slideProps.takeover && this.props.slideProps.takeover.src
                }
            ])}>
                <PaddleLeft 
                    visible={this.state.paddles.left.visible}
                    slideLeft={this.slideLeft.bind(this)}
                />
                <PaddleRight
                    visible={this.state.paddles.right.visible}
                    slideRight={this.slideRight.bind(this)}
                />
                <PaddleLoading 
                    visible={this.state.paddles.loading.visible}
                />
                <div data-sigil="peek-close" className={classnames([
                    'peek-close',
                    {'back': this.props.slideProps.isNested}
                ])}></div>
                <div className="peek-slides">
                    {slides.map((slide) => {
                        return <Slide 
                                    key={slide.id}
                                    nodeMetadata={slide.nodeMetadata}
                                    position={slide.position}
                                    visible={ this.props.isVisible }
                                    {...slideProps}
                                    />
                    })}
                </div>
            </div>
        );
    }
}

module.exports = PeekContainer;
