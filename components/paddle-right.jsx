'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class PaddleRight extends ReactBase {
    render() {
        const classes = ['peek-direction', 'peek-right'];

        if (this.props.visible) {
            classes.push('visible');
        }

        return (<div data-sigil="peek-noclose" className={classes.join(' ')} onClick={this.props.slideRight} />)
    }
}

module.exports = PaddleRight;