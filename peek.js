'use strict';

const extend = require('lib/data/extend');
const Meta = require('lib/data/meta');
const sigil = require('lib/dom/sigil');
const listener = require('lib/dom/event-listener');
const modal = require('lib/da/modal');
const Hotkeys = require('lib/da/hotkeys');

const Container = require('lib/da/peek/components/container');

const meta = Meta.createLocal('peek');

// keep track of how many peeks we have
let nextId = 0;

// so that we only attach one listener event for each sigil
const sigilsListedTo = [];
let windowkeyDownEventAttached = false;

// a global repository of peeks so we can open the correct one
// when a node is clicked on
const peeks = {};

// a stack of open peeks
// we keep this in tandem with modals so that we know which peeks
// to operate on for global key handlers
const openPeeks = [];

const openPeek = (e) => {
    // option click? let's get out of here
    if (e.originalEvent.metaKey) {
        return;
    }
    // not a left click? give up too
    if (e.originalEvent.which != 1) {
        return;
    }

    // allow usernames to go to profile page
    if (e.target && e.target.classList.contains('username')) {
        return;
    }

    // no, you can't just visit the deviation page
    e.preventDefault();
    e.stopPropagation();

    const data = meta.get(e.node);

    // we don't have any data, maybe you messed up?
    if (data.id === undefined) {
        console.warn('peek sigil clicked, but no peek id set', data, e.node);
        return;
    }

    if (!windowkeyDownEventAttached) {
        windowkeyDownEventAttached = true;
        Hotkeys.register(['LEFT'], slideCurrentLeft);
        Hotkeys.register(['RIGHT'], slideCurrentRight);
    }
    
    peeks[data.id].openAt(e.node);
}

const createPeek = (wrapper, sigil, slideProps) => {
    if (sigilsListedTo.indexOf(sigil) == -1) {
        listener.on('touchstart', sigil, openPeek);
        listener.on('click', sigil, openPeek);
        sigilsListedTo.push(sigil);
    }

    return new Peek(wrapper, sigil, extend(slidePropsDefault, slideProps));
}

// needs to be declared after `createPeek`
const slidePropsDefault = {
    // this is added to the backdrop
    // should be an object containing at least 'src' and optionally 'url'
    // also allows a callback for when the background is clicked
    takeover: null,
    // should the peek have a header attached to it?
    // this is a ReactComponent that is rendered on top of the slide
    header: null,
    // should the save button be available
    hideSaveButton: false,
    // is this a nested peek
    isNested: false,
    // anythign that opens a peek from inside peek needs this as 
    // we can't just require this file from something this requires -- cirucular dependencies
    peek: createPeek
};

const current = () => {
    return peeks[openPeeks[openPeeks.length - 1]];
}

const slideCurrentLeft = () => {
    const activeElem = document.activeElement;
    if (
        activeElem.value === undefined
        && activeElem.contentEditable !== 'true'
        && current().isCurrentPopup()
    ) {
        current().slideLeft();
    }
}

const slideCurrentRight = () => {
    const activeElem = document.activeElement;
    if (
        activeElem.value === undefined
        && activeElem.contentEditable !== 'true'
        && current().isCurrentPopup()
    ) {
        current().slideRight();
    }
}

const peekContainerClick = (e) => {
    if (!sigil.closest(e.target, 'peek-noclose')) {
        current().hide();
    }
}

const peekHidden = () => {
    const peek = peeks[openPeeks.pop()];
    peek.unslideable();

    if (!openPeeks.length) {
        peek.scrollToLast();

        windowkeyDownEventAttached = false;
        Hotkeys.unregister(['LEFT'], slideCurrentLeft);
        Hotkeys.unregister(['RIGHT'], slideCurrentRight);
    }
}

class Peek {
    constructor(wrapper, sigil, slideProps) {
        this.wrapper = wrapper;
        this.sigil = sigil;
        this.id = nextId++;
        this.takenover = slideProps.takeover;

        this.modal = modal.create(Container, {
            position: false,
            backgroundTakeover: slideProps.takeover,
            props: {slideProps: slideProps}
        });

        // index this peek so we can open when clicked
        peeks[this.id] = this;

        this.modal.on('hide', peekHidden);
    }
    _actions() {
        return this.modal.getComponent().alt.getActions('PeekContainerActions');
    }
    updateNodes(reset) {
        this.setPeekIdOnNodes();
        this._actions().updateNodes({
            wrapper: this.wrapper,
            sigil: this.sigil,
            reset: reset
        });
    }
    setPeekIdOnNodes() {
        const thumbs = sigil.find(this.wrapper, this.sigil);
        for (let i = 0; i < thumbs.length; i++) {
            meta.set(thumbs[i], {id: this.id});
        }
    }
    openAt(node) {
        openPeeks.push(this.id);
        this._actions().openAt(node);
        this.modal.show();
    }
    slideRight() {
        this._actions().slideRight();
    }
    slideLeft() {
        this._actions().slideLeft();
    }
    hide() {
        this.modal.hide();
    }
    scrollToLast() {
        this._actions().scrollToLast();
    }
    unslideable() {
        this._actions().unslideable();
    }
    on(event, fn) {
        this.modal.getComponent().alt.getStore('PeekContainerStore').getEventEmitter().on(event, fn);
    }
    delete() {
        this.modal.delete();
    }
    isCurrentPopup() {
        return this.modal.getComponent().props.isCurrentPopup;
    }
}

listener.on('click', 'peek-container', peekContainerClick);

module.exports = {
    /**
     * Creates a peek instance
     * @param {HTMLElement} wrapper - The wrapper element with the thumbs inside of it from which peek is created
     * @param {String} sigil - The sigil on the thumbs from which peek is created
     * @param {object} slideProps - props applied to all slides within the peek instance
     */
    create: function(wrapper, sigil, slideProps = {}) {
        return createPeek(wrapper, sigil, slideProps);
    }
}
