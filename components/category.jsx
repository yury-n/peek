'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class Category extends ReactBase {
    render() {
        const categoryPaths = [];
        const categoryLinks = [];

        let categoryPathParts = this.props.categoryPath.split("/");
        for (let i = 0; i < categoryPathParts.length; i++) {
            categoryPaths.push(categoryPathParts[i]);
            categoryLinks.push(<a key={i} href={ "http://www.deviantart.com/browse/all/" + categoryPaths.join('/') }>{ this.props.categoryPathTitles[i] }</a>);
        }

        return <div className="cats">{ categoryLinks }</div>;
    }
}

module.exports = Category;