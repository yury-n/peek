'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class FaveDropdownItem extends ReactBase {
    handleClick() {
        this.props.faveDropdownSelect(this.props.folderid, this.props.name)

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'collection_listitem',
            section: 'right_column',
            component: 'collection_dropdown',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        return (
             <li>
                <a href="javascript:void(0)" className={classnames({
                    'special': this.props.subtitle,
                    'creating': this.props.folderid === 0
                })} onClick={this.handleClick}>
                    <i style={{backgroundImage: "url('" + this.props.icon + "')"}}></i>
                    <strong>{this.props.name}</strong>
                    {this.props.subtitle}
                </a>
            </li>
       );
    }
}

module.exports = FaveDropdownItem;
