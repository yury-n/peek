'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const popup = require('lib/da/popup');

let reportedID = 0;

class reportPopup extends ReactBase {

    hide() {
        this.props.hide();
    }
    render() {
        let devID = reportedID;
        let frameSrc = '//www.deviantart.com/deviation/report/' + devID + '?is_modal=true'

        return (
            <div className="moderation-modal">
                <div className="moderation-head">
                        <iframe src={ frameSrc } frameBorder="0" height="600" width="620"></iframe>
                        <img className="fella-cop" src="http://st.deviantart.net/moderation/police.gif" />
                </div>
                <div className="moderation-modal-buttons">
                    <a className="smbutton smbutton-size-default smbutton-shadow smbutton-lightgreen moderation-modal-buttons-cancel" onClick={ this.hide }>
                        Cancel
                    </a>
                    <div className="clear" />
                </div>
            </div>
       );
    }
}

class reportButton extends ReactBase {
    constructor(props) {
        super(props);
        this.reportPopup = false;
    }
    
    handleReportClick() {
        if (!this.reportPopup) {
            reportedID = this.props.data.id;
            this.reportPopup = popup.create(reportPopup, 
                    {fixed: true, 
                     position: true, 
                     hideOnEscape: true,
                     displayBackdrop: true
                    });
        }
        this.reportPopup.show();
    }

    render() {
        return (
            <span className="report-button">
                <a className="flag" onClick={this.handleReportClick}>
                    <span className='tooltip'>Report This Deviation</span>
                </a>
            </span>
        );
    }
}

module.exports = reportButton;
