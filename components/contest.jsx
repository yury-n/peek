'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class ContestExtras extends ReactBase {

    render() {
        return (
            <div className="contest" style={{backgroundImage : 'url(' + this.props.data.logo +' )'}}>
                <div className="btnWrapper">
                    <a href={this.props.data.entry_url} className="contestBTN">
                        { this.props.entryButtonText ? this.props.entryButtonText : 'Enter Contest' }
                    </a>
                    <a href={this.props.data.category_url} className="contestBTN">Browse Entries</a>
                </div>
            </div>
       );
    }
}

module.exports = ContestExtras;
