'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');

class ShareButton extends ReactBase {
    render() {
        return (
            <div className={classnames(['share', 'btn', 'dark', 'good', {'open': this.props.isOpen}])} {...this.props} title="Share">
                <span className="icon-share-fill share-button-icon" />
            </div>
            )
    }
}

module.exports = ShareButton;

