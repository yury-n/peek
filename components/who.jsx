'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const popup = require('lib/da/popup');

let deviation_id = 0;
let deviation_title = '';

class whoPopup extends ReactBase {

    hide() {
        this.props.hide();
    }

    render() {
        return (
            <div className="who-modal">
                <div className="who-modal-content">
                    <div className="who-modal-box">
                        <a href="javascript:void(0)" className="who-modal-close" onClick={this.hide}></a>
                        <h1>{deviation_title}</h1>
                        <div className="who-modal-subtitle">The following users have added this deviation to their favourites.</div>
                            <iframe src={'//www.deviantart.com/deviation/' + deviation_id + '/favourites'}  frameBorder="0" height="450" width="620"></iframe>
                    </div>
                </div>
            </div>
       );
    }
}

class whoButton extends ReactBase {
    constructor(props) {
        super(props);
        this.popup = false;
    }
    
    handleWhoClick() {
        deviation_id = this.props.id;
        deviation_title = this.props.title;

        if (!this.popup && deviation_id) {
            this.popup = popup.create(whoPopup, {
                position: true,
                fixed: true,
                hideOnEscape: true,
                displayBackdrop: true,
                forceDimensions: false,
            });
        }
        this.popup.show();
    }

    render() {
        return (<a href="javascript:void(0)" onClick={this.handleWhoClick}>who?</a>);
    }
}

module.exports = whoButton;
