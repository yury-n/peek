'use strict';

const statusCache = require('lib/da/cache/status');
const deviationCache = require('lib/da/cache/deviation');

// Actions that are common between the NotePopup and StatusPopup
class SharePopupActions {
    constructor() {
        this.generateActions('resetForm', 'setCollectionData');
    }

    /**
     * fetches status content from status cache
     */
    fetchStatusContent(statusid, userid) {
        if (!statusid) {
            this.dispatch(false);
        } else {
            statusCache.content(statusid, userid).then((data) => {
                this.dispatch(data);
            }).catch((e)=>{console.log(e)});
        }
    }
    /**
     * fetches deviation metadata from cache
     * @param {int} id The deviation id
     */
    fetchDeviationMetadata(id) {
        if (!id) {
            this.dispatch(false);
        } else {
            deviationCache.metadata(id).then((data) => {
                this.dispatch(data);
            }).catch((e)=>{console.log(e)});
        }
    }
}

module.exports = SharePopupActions;
