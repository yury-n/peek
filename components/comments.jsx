'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const UserIcon = require('lib/da/user/components/user-icon');
const Loading = require('lib/da/peek/components/loading');
const Paginator = require('lib/da/components/paginator');
const deviation = require('lib/da/cache/deviation');
const DA = require('lib/da/deviantart-object');
const sigil = require('lib/dom/sigil');
const classnames = require('lib/tp/classnames/classnames');

const comments = require('lib/da/comments');
const Comment = require('lib/da/peek/components/comment');
const CommentBox = require('lib/da/peek/components/commentbox');

const emptyMessages = [
    "What catches your eye about this deviation?",
    "How could this artist improve on their skill?",
    "Does this deviation inspire you?",
    "What three things do you like most about this deviation? What's one thing that could be improved?"
];

class Comments extends ReactBase {
    constructor(props) {
        super(props);

        this.state = {
            emptyMessage: emptyMessages[Math.floor(Math.random() * emptyMessages.length)]
        }
    }
    page(offset) {
        this.props.actions.paginateClick(this.props.type, this.props.id, offset);
    }

    postComment(type, id, ownerid, body, parentid) {
        this.props.actions.postComment(type, id, ownerid, body, parentid);
    }

    onChange() {
        this.setState(this.store.getState());
    }

    getComments() {
        const comments = this.props.comments;

        if (!comments || comments.thread.length == 0) {
            return (<div className="empty">{this.state.emptyMessage}</div>);
        }

        const commentList = [];
        let depth = 0;

        for (let i = 0; i < comments.thread.length; i++) {
            const item = comments.thread[i];
            let itemDepth = depth;

            // Time to work out what depth this comment is nested at.
            // This comment has a parent, so it's deeper than its parent was.
            if (item.parentid) { 
                itemDepth++; 

                if (item.replies > 0) {
                    // If the child comment has more replies set the "parent depth" to the depth of this comment.
                    depth = itemDepth;
                }
            } else {
                // if the comment has no parent it's root level, and all the depths need to be reset.
                itemDepth = 0;
                depth = 0; 
            }

            commentList.push( <Comment 
                                    actions={ this.props.actions }
                                    comment={ item } 
                                    depth={ itemDepth } 
                                    isCommenting={ this.props.isCommenting } 
                                    itemid={ this.props.id }
                                    key={ i }
                                    ownerid={ this.props.ownerid }
                                    postComment={ this.postComment }
                                    previewToggle={ this.previewToggle }
                                    type={ this.props.type }
                                    getDapxExtraData={ this.props.getDapxExtraData }
                                />
                            );
        }
        return commentList;
    }

    generateComments() {
        if (!this.props.contentLoaded) {
            return <Loading />;
        }

        const commentList = this.getComments();
        let commentExtra = false;
        let commentStart;

        if (Array.isArray(commentList)) {
            commentStart = commentList.slice(0, 2);
            commentExtra = commentList.slice(2);
        } else {
            commentStart = commentList;
        }
        
        return (<div className="comments">
                    { commentStart }
                    <div className="extras">
                        { commentExtra }
                    </div>
                </div>);
    }

    generateCommentButtons() {
        if (this.props.page == 0 && (!this.props.comments || this.props.comments.thread.length == 0)) {
            return false;
        }

        let paginator = false;
        if (this.props.commentViewActive) {
            paginator = <Paginator count={ this.props.commentCount } callback={ this.page } page={ this.props.page } />;
        }

        return (<div className="more-comments">
                    { paginator }
                </div>);
    }

    render() {
        if (this.props.allowsComments === false) {
            return (
                <div className="comment-block">
                    <div className="empty">The owner of this deviation has disabled comments.</div>
                </div>
            );
        }

        return (
                <div className="comment-block">
                    <CommentBox 
                        isCommenting={ this.props.isCommenting }
                        isVisible={ true }
                        id={ this.props.id }
                        itemid={ this.props.id }
                        type={ this.props.type }
                        postComment={ this.postComment }
                        ownerid={ this.props.ownerid }
                        getDapxExtraData={ this.props.getDapxExtraData }
                    />
                    { this.generateComments() }
                    { this.generateCommentButtons() }
                </div>
       );
    }
}

module.exports = Comments;
