'use strict';

const React = require('lib/tp/react');
const UserLink = require('lib/da/user/components/user-link');
const UserIcon = require('lib/da/user/components/user-icon');
const InfoStatus = require('lib/da/peek/components/info/status');
const TabLinksContainer = require('lib/da/peek/components/tab-links-container');
const Share = require('lib/da/peek/components/share');

class InfoCollection extends InfoStatus {
    generateUsericon() {
        return (
            <UserIcon
                className="artist-icon"
                username={ this.props.nodeMetadata.curator.username }
                usericon={ this.props.nodeMetadata.curator.usericon } />
            );
    }

    generateUserlink() {
        return <UserLink className="artist-name" username={ this.props.nodeMetadata.curator.username } />;
    }

    generateSubmitted() {
        return false;
    }

    generateCopyright() {
        return false;
    }

    generateShare() {
        return (<Share
            id={ this.props.nodeMetadata.id }
            author={ this.props.curator }
            isOpen={ this.props.shareOpen }
            type={ this.props.type }
            sizing={ this.props.nodeMetadata.sizing }
            collection={ this.props.nodeMetadata.collection_data }
            url={ this.props.url }
            title={ this.props.title }
            description={ this.props.truncatedDescription }
            triangleLeft={ this.getTriangleLeft() }
        />);
    }
}

module.exports = InfoCollection;
