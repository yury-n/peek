'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const sigil = require('lib/dom/sigil');
const deviation = require('lib/da/cache/deviation');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');
const gallection = require('lib/da/gallection');
const THUMB_CLASS = require('lib/da/torpedo/gallection/constants/thumb-class');
const DeviationTypes = require('lib/da/peek/constants/deviation-types');

const Alt = require('lib/tp/alt');
const Actions = require('lib/da/peek/actions/slide');
const Store = require('lib/da/peek/stores/slide');

const DeviationObject = require('lib/da/peek/components/deviation');
const CollectionObject = require('lib/da/peek/components/collection');
const PollObject = require('lib/da/peek/components/poll');
const StatusObject = require('lib/da/peek/components/status');
const Info = require('lib/da/peek/components/info');
const Comments = require('lib/da/peek/components/comments');
const Loading = require('lib/da/peek/components/loading');

const DailyDeviationExtras = require('lib/da/peek/components/dds');
const ContestExtras = require('lib/da/peek/components/contest');
const ChallengeExtras = require('lib/da/peek/components/challenge');
const CampaignExtras = require('lib/da/peek/components/campaignBanner');

class Slide extends ReactBase {
    constructor(props) {
        super(props);
        this.alt = new Alt();
        this.alt.addActions('SlideActions', Actions);
        this.alt.addStore('SlideStore', Store);

        this.state = this.getStore().getState();
    }

    getStore() {
        return this.alt.getStore('SlideStore');
    }

    getActions() {
        return this.alt.getActions('SlideActions');
    }

    componentDidMount() {
        this.setScrollableContent();
        this.getStore().listen(this.onChange);
        this.getActions().fetchContent(this.props.nodeMetadata);
        this.getActions().slideMount();
        this.container = sigil.first('peek-slide-' + this.props.nodeMetadata.id);

        // listen to ThumbClassStore for faved/saved state updates, but only if
        // this is a deviation slide
        if (
            this.props.nodeMetadata.type == 'image'
            || this.props.nodeMetadata.type == 'freeform'
        ) {
            this.listeningToThumbClassStore = true;
            gallection.thumbClassStore.listen(this.updateFavedSavedState);
        }
    }
    
    componentWillUnmount() {
        this.getStore().unlisten(this.onChange);
        this.getActions().slideUnmount();

        if (this.listeningToThumbClassStore) {
            gallection.thumbClassStore.unlisten(this.updateFavedSavedState);
        }
    }

    onChange() {
        this.setState(this.getStore().getState());
    }

    // This addresses an edge case in which a peek slide has rendered with a
    // given faved/saved state but that state was later updated from outside of
    // this slide and this slide has yet to unmount. If it had unmounted, the
    // cache would be invalidated and a subsequent slide render of the deviation
    // in this slide would have the correct state. But because this never
    // unmounted it is left with the previous state, so figure out what the new
    // state should be and force it. But wait a tick to do so as we likely need
    // to let the previous render finish.
    updateFavedSavedState(storeState) {
        if (this.state.contentLoaded && this.props.nodeMetadata.id in storeState.thumbClassById) {
            window.requestAnimationFrame(() => {
                const correctedThumbClass = storeState.thumbClassById[this.props.nodeMetadata.id];
                let newState;
                switch (correctedThumbClass) {
                    case THUMB_CLASS.FAVED:
                        if (!this.state.isFavourited) {
                            newState = {
                                isFavourited: true,
                                isSaved: false
                            };
                        }
                        break;
                    case THUMB_CLASS.SAVED:
                        if (!this.state.isSaved) {
                            newState = {
                                isFavourited: false,
                                isSaved: true
                            };
                        }
                        break;
                    case false:
                        if (this.state.isFavourited || this.state.isSaved) {
                            newState = {
                                isFavourited: false,
                                isSaved: false
                            };
                        }
                        break;
                }
                if (newState) {
                    this.getActions().forceState(newState);
                }
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        // we also want to update ourselves if we're sliding anywhere
        if (nextProps.position != this.props.position) {
            return true;
        }
        
        // this works because it always renders on construct
        // and then any updates that don't have the contentLoaded we don't care about
        return nextState.contentLoaded;
    }

    componentDidUpdate(prevProps, prevState) {
        // if this slide has moved into the center position ensure it's scroll
        // position is at the top to start
        if (
            this.props.position != prevProps.position
            && this.props.position == 0
        ) {
            if (this.scrollableElement) {
                // If there is a scrollable element, focus on that.
                this.scrollableElement.focus();
            } else {
                // Otherwise, focus the entire slide for keyboard scrolling.
                ReactDOM.findDOMNode(this.refs.slide).focus();
            }
            ReactDOM.findDOMNode(this.refs.slide).scrollTop = 0;
        }

        // log a view event every time a slide with loaded content has entered
        // the center position
        if (
            this.state.contentLoaded
            && this.props.position === 0
            && (
                this.props.position !== prevProps.position
                || !this.logged
            )
        ) {
            // THIS IS CONSIDERED A PEEK "VIEW"

            // we have to set a `logged` flag so subsequent updates after entering the
            // centered position don't record a new view
            this.logged = true;

            this.getActions().logDeviationPageView(this.props.nodeMetadata);
            this.logPeekView();
            this.insertClientTrackingPixel();
        }
    }

    logPeekView() {
        let element = this.props.nodeMetadata.type + '_peek';
        // if this peek is nested from within a collection the `element`
        // needs to reflect that
        if (this.props.isNested && this.props.header) {
            element = 'collection-' + element;
        }
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: element,
            action: 'view',
        }, this.getDapxExtraData());
    }

    // TODO this could probably be better, but in the interest of time...
    // Some clients want to use their own tracking pixel to record peek views
    // from within a campaign journal. Use the campaignKey prop to identify
    // these views and insert the appropriate pixel HTML.
    insertClientTrackingPixel() {
        let pixel = false;
        switch (this.props.campaignKey) {
            // T26887
            case 'SONY_METROPOLIS_2015':
                pixel = '<IMG SRC="https://ad.doubleclick.net/ddm/ad/N7110.131920DEVIANTART.COM/B8851001.122311859;sz=1x1;ord=' + Date.now() + '?" BORDER=0 WIDTH=1 HEIGHT=1 ALT="Advertisement">';
                break;
        }
        if (pixel) {
            // in order to insert this tracking pixel we have to update state,
            // but this is being called from componentDidUpdate, so wait a tick
            // then call the appropriate action
            window.requestAnimationFrame(() => {
                this.getActions().setTrackingPixelHTML(pixel)
            });
        }
    }

    // Extra data passed in with all dapx events. Should include `context`,
    // `itemtype`, `itemid`, and `itemowner`.
    getDapxExtraData() {
        const extraData = {}

        // default
        extraData.itemtype = this.props.nodeMetadata.type;
        extraData.itemid = this.props.nodeMetadata.id;
        extraData.itemowner = this.props.nodeMetadata.author.userid;
        extraData.context = this.props.context || 'watch';

        // override itemowner if there is a curator set
        if (this.props.nodeMetadata.curator) {
            extraData.itemowner = this.props.nodeMetadata.curator.userid;
        }

        // campaign journals have a campaignJournalId prop and must be given as
        // the `context`
        if (this.props.campaignJournalId) {
            extraData.context = this.props.campaignJournalId;
        }

        return extraData;
    }

    moreCommentsClick() {
        this.getActions().moreCommentsClick();
    }
    
    setScrollableContent() {
        this.scrollableElement = sigil.first(ReactDOM.findDOMNode(this), 'peek-scrollable-content');
        if (this.scrollableElement) {
            // ensure scrollable element has a tabIndex so we are able to focus it later
            this.scrollableElement.tabIndex = -1;
        }
    }
    
    getCommentItemid() {
        if (this.props.nodeMetadata.type == 'collection') {
            return this.props.nodeMetadata.collection_data.gallection_meta.gallectionid;
        } else {
            return this.props.nodeMetadata.id;
        }
    }

    generateCoreContent() {
        if (this.props.nodeMetadata.type == 'collection') {
            return <CollectionObject 
                            actions={ this.getActions() }
                            deviations={ this.state.deviations }
                            contentLoaded={ this.state.contentLoaded }
                            gridLoaded={ this.state.collectionGridLoaded }
                            expanded={ this.state.expanded }
                            url={ this.state.url }
                            nodeMetadata={ this.props.nodeMetadata }
                            peek={ this.props.peek }
                            position={ this.props.position }
                        />;
        }

        if (this.props.nodeMetadata.type == 'poll') {
            return <PollObject 
                            actions={ this.getActions() }
                            contentLoaded={ this.state.contentLoaded }
                            url={ this.state.url }
                            nodeMetadata={ this.props.nodeMetadata }
                            poll={ this.state.poll }
                            publishedTime={ this.state.publishedTime }
                            vote={ this.getActions().vote }
                            isVoting={ this.state.isVoting }
                        />;
        }

        if (this.props.nodeMetadata.type == 'status') {
            return <StatusObject 
                            actions={ this.getActions() }
                            contentLoaded={ this.state.contentLoaded }
                            nodeMetadata={ this.props.nodeMetadata }
                            status={ this.state.status }
                            position={ this.props.position }
                            visible={ this.props.visible }
                        />;
        }

        if (DeviationTypes.test(this.props.nodeMetadata.type)) {
            return <DeviationObject 
                                actions={ this.getActions() }
                                contentLoaded={ this.state.contentLoaded }
                                nodeMetadata={ this.props.nodeMetadata }
                                freeformContent={ this.state.freeformContent }
                                header={ this.props.header }
                                takeover={ this.props.takeover }
                                status={ this.state.status }
                                position={ this.props.position }
                                visible={ this.props.visible }
                            />;
        }

        console.error('unknown slide type', this.props.nodeMetadata.type);
    }

    generateExtraContainer() {
        // if we're still loading everything in, just return a generic "loading"
        if (!this.state.contentLoaded) {
            return <Loading />;
        }

        // otherwise we'll return the whole enchilada
        return (<div>
                    {this.state.dailyDevaition ? <DailyDeviationExtras data={ this.state.dailyDevaition } /> : false}
                    {this.state.contestEntry ? <ContestExtras data={ this.state.contestEntry } entryButtonText={ this.props.entryButtonText } /> : false}
                    {this.state.challengeEntry ? <ChallengeExtras data={ this.state.challengeEntry } /> : false}
                    {this.props.campaignKey ? <CampaignExtras campaignKey= {this.props.campaignKey } getDapxExtraData={ this.getDapxExtraData } /> : false}
                    <div className="extras-tabs">
                        { this.generateCommentClose() }
                    </div>
                    <Comments 
                        actions={ this.getActions() }
                        id={ this.getCommentItemid() }
                        comments={ this.state.comments }
                        allowsComments={ this.state.allowsComments }
                        isCommenting={ this.state.isCommenting }
                        ownerid={ this.state.ownerid }
                        type={ this.props.nodeMetadata.type }
                        contentLoaded={ this.state.contentLoaded }
                        commentViewActive={ this.state.commentViewActive }
                        commentCount={ this.state.stats.comments }
                        page={ this.state.page }
                        getDapxExtraData={ this.getDapxExtraData }
                    />
                    <div className="extras-buttons">
                        { this.generateCommentListButton() }
                    </div>
                </div>);
    }

    generateLeftColumn() {
        return (<div>
                <div className="slide-core-content" >
                { this.generateCoreContent() }
                </div>

                <div className="extras-container">
                { this.generateExtraContainer() }
                </div>
                </div>);
    }

    generateRightColumn() {
        if (!this.state.contentLoaded) {
            return null;
        }

        return <Info
            actions={ this.getActions() }
            nodeMetadata={ this.props.nodeMetadata }
            peek={ this.props.peek }
            type={ this.props.nodeMetadata.type }
            author={ this.state.author || this.props.nodeMetadata.author }
            curator={ this.props.nodeMetadata.curator }
            id={ this.props.nodeMetadata.id }
            activeTab={ this.state.activeTab }
            publishedTime={ this.state.publishedTime }
            url={ this.state.url }
            title={ this.state.title }
            stats={ this.state.stats }
            descriptionCollapsed={ this.state.descriptionCollapsed }
            tagsCollapsed={ this.state.tagsCollapsed }
            truncatedDescription={ this.state.truncatedDescription }
            relatedArt={ this.state.relatedArt }
            shareOpen={ this.state.shareOpen }
            isFavourited={ this.state.isFavourited }
            isFaving={ this.state.isFaving }
            isFaveDropdownActive={ this.state.isFaveDropdownActive }
            allowsComments={ this.state.allowsComments }
            downloadUrl={ this.state.downloadUrl }
            isSaved={ this.state.isSaved }
            isSaving={ this.state.isSaving }
            contentLoaded={ this.state.contentLoaded }
            categoryPath={ this.state.categoryPath }
            categoryPathTitles={ this.state.categoryPathTitles }
            tags={ this.state.tags }
            status={ this.state.status }
            expandComments={ this.getActions().moreCommentsClick }
            collections={ this.state.collectionsData.collections }
            collectionsLoaded={ this.state.collectionsData.loaded }
            isNested={ this.props.isNested }
            newCollection={ this.state.newCollection }
            position={ this.props.position }
            hideSaveButton={ this.props.hideSaveButton }
            getDapxExtraData={ this.getDapxExtraData }
        />
    }

    handleViewAllClick() {
        this.moreCommentsClick();

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'view_all_btn',
            section: 'left_column',
            component: 'comments',
            action: 'click'
        }, this.getDapxExtraData());
    }

    handleViewXCommentsClick() {
        this.moreCommentsClick();

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'view_x_cmnts_btn',
            section: 'left_column',
            component: 'comments',
            action: 'click'
        }, this.getDapxExtraData());
    }

    generateCommentClose() {
        if (this.state.allowsComments === false) {
            return <span className="comment-count">Comments Disabled</span>;
        }
        if (this.state.commentViewActive) {
            return <span className="closeTab" onClick={ this.moreCommentsClick }>Close</span>
        }
        if (this.state.stats.comments > 2) {
            return (
                <div>
                    <span className="mini-button" onClick={ this.handleViewAllClick }>View All</span>
                    <span className="comment-count">{ this.state.stats.comments } Comments</span>
                </div>
                );
        }
        return <span className="comment-count">Comments</span>;
    }

    generateCommentListButton() {
        if (!this.state.commentViewActive && this.state.allowsComments !== false) {
            if (this.state.stats.comments > 2) {
                return <span className="view-button" onClick={ this.handleViewXCommentsClick }>View All { this.state.stats.comments } Comments</span>;
            }
        }
        return false;
    }

    generateTrackingPixel() {
        if (this.state.trackingPixelHTML) {
            return <span dangerouslySetInnerHTML={{__html: this.state.trackingPixelHTML}} />;
        }
    }

    render() {
        const translateValue = this.props.position * 100;
        const slideStyle = {
            // we use translateX here instead of translate3D because
            // 3D makes the contents of the slide blurry for some reason
            // if this ever changes or gets better, it might be better to use 3D here
            transform: 'translateX(' + translateValue + 'vw)',
            WebkitTransform: 'translateX(' + translateValue + 'vw)',
            MozTransform: 'translateX(' + translateValue + 'vw)'
        };

        return (
            <div className={ classnames([
                        'peek-slide',
                        {
                            center: this.props.position === 0,
                            next: Math.abs(this.props.position) == 1,
                            zoomed: this.state.zoomed,
                            comments: this.state.commentViewActive,
                            thumbOpen: this.props.header
                        }
                ])}
                style={slideStyle}
                data-sigil={ 'peek-slide-' + this.props.nodeMetadata.id }
                ref="slide"
                tabIndex="1">
                <div data-sigil="peek-noclose" className="peek-main-component">
                    <div className="left-column">
                        { this.generateLeftColumn() }
                    </div>
                    <div className="right-column">
                        { this.generateRightColumn() }
                        { this.generateTrackingPixel() }
                    </div>
                </div>
            </div>)
    }
}

module.exports = Slide;
