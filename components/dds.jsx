'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const dateFormatter = require('lib/da/date-formatter');
const UserLink = require('lib/da/user/components/user-link');

class DailyDeviationExtras extends ReactBase {
    render() {
        return (
            <div className="dds">
                <div className="title">
                    <h1>Daily Deviation</h1>
                    <h2>Given {dateFormatter.mdy(new Date(this.props.data.time))}</h2>
                </div>
                <div className="content" dangerouslySetInnerHTML={{__html: this.props.data.body }} />
                <div className="featuredBy">
                    <span>Featured by <UserLink username={this.props.data.giver.username} /></span>
                    {this.props.data.suggester ?
                        <span>Suggested by <UserLink username={this.props.data.suggester.username} /></span> :
                        false
                    }
                </div>
            </div>
       );
    }
}

module.exports = DailyDeviationExtras;
