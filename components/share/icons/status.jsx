'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class StatusIcon extends ReactBase {
    handleClick() {
        if (!this.props.disabled) {
            this.props.openStatusModal();
        }

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'da_share',
            section: 'right_column',
            component: 'share_options',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        return (
            <div className="share-icon-wrap">
                <a href="javascript:void(0)" onClick={this.handleClick} className={classnames([
                    'share-icon',
                    'share-status',
                    {'disabled': this.props.disabled}
                ])}>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="300px" height="300px" viewBox="0 0 300 300" enable-background="new 0 0 300 300">
                        <g>
                            <polygon fill="#05CC47" points="202.686,95.335 202.686,64.36 202.678,64.36 171.711,64.36 168.618,67.478 154.002,95.332 149.407,98.434
            97.239,98.434 97.239,140.972 125.919,140.972 128.472,144.06 97.239,203.745 97.239,234.72 97.25,234.72 128.214,234.72
            131.307,231.605 145.923,203.748 150.518,200.649 202.686,200.649 202.686,158.108 174.006,158.108 171.456,154.999 "></polygon>
                        </g>
                    </svg>
                </a>
            </div>
            );
    }
}

module.exports = StatusIcon;
