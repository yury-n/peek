'use strict';

const React = require('lib/tp/react');
const InfoBase = require('lib/da/peek/components/info/base');
const TabLinksContainer = require('lib/da/peek/components/tab-links-container');
const Share = require('lib/da/peek/components/share');
const RelatedArt = require('lib/da/peek/components/related-art');

class InfoStatus extends InfoBase {
    // intentially empty, just need it as a placeholder to not do anything
    updateTorpedos() {}

    generateFaveButton() {
        return false;
    }

    generateTabLinks() {
        return (
            <TabLinksContainer
                tabs={ ['info'] }
                changeActiveTab={ this.handleTabClick.bind(this) }
                activeTab={ this.props.activeTab } />
            );
    }

    generateInfoViews() {
        return false;
    }

    generateInfoFaves() {
        return false;
    }

    generateDescription() {
        return false;
    }

    generateReportButton() {
        return false;
    }

    generateStats() {
        return false;
    }

    getTriangleLeft() {
        return '9px';
    }

    generateShare() {
        let id = this.props.nodeMetadata.id;
        let author = this.props.author;
        let type = this.props.type;

        // we actually want the info of a status's attachment if we have that
        if (this.props.status && this.props.status.is_share) {
            type = this.props.status.items[0].type;
            const share = this.props.status.items[0][type];

            if (share.is_deleted) {
                // this is the "most correct" thing we can do here
                // as it is not guaranteed that the author of anything
                // we have so far is the actual author here
                author = {};
                id = 0;
            } else {
                id = share.deviationid || share.statusid;
                author = share.author;

                // statuses are jerks and give us an RID instead of a number
                if (id.id) {
                    id = id.id;
                }
            }
        }

        return (<Share
            id={ id }
            author={ author }
            isOpen={ this.props.shareOpen }
            type={ type }
            sizing={ this.props.nodeMetadata.sizing }
            url={ this.props.url }
            title={ this.props.title }
            description={ this.props.truncatedDescription }
            triangleLeft={ this.getTriangleLeft() }
        />);
    }
}

module.exports = InfoStatus;
