'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class TumblrIcon extends ReactBase {
    isDisabled() {
        return !this.props.url;
    }
    handleClick() {
        if (!this.isDisabled()) {
            let url = 'http://www.tumblr.com/share';
            // determine if we're sharing a link or an image and build the url appropriately
            // Link
            if (
                this.props.type == 'status'
                || this.props.type == 'freeform'
                || !this.props.src
            ) {
                let description = '<p><small>by <a href="http://' + this.props.author.username + '.deviantart.com">' + this.props.author.username + '</a></small></p>';
                if (this.props.description) {
                    description += this.props.description + '<p><a href="' + this.props.url + '">Read more</a></p>';
                }
                url += '/link?';
                url += 'url=' + encodeURIComponent(this.props.url);
                url += '&name=' + encodeURIComponent(this.props.title);
                url += '&description=' + encodeURIComponent(description);
            }
            // Image
            else {
                const caption = '<a href="' + this.props.url + '">' + this.props.title + '</a> by ' + '<a href="http://' + this.props.author.username + '.deviantart.com">' + this.props.author.username + '</a>';
                url += '/photo?';
                url += 'source=' + encodeURIComponent(this.props.src);
                url += '&caption=' + encodeURIComponent(caption);
                url += '&clickthru=' + encodeURIComponent(this.props.url);
            }

            const popup = window.open('about:blank', 'sharetumblr', 'toolbar=0,status=0,resizable=yes,width=450,height=480');
            if (!popup) {
                return;
            }

            // Tumblr is using HTTP referrer to show the source of the post being shared.
            // We don't want to provide the referrer for 2 reasons:
            // 1) post caption already contains a link to the deviation,
            // 2) When sharing from peek Tumblr receives incorrect referrer.
            // To get rid of referrer we first open a blank window and then redirect it to Tumblr.
            popup.document.open();
            popup.document.write('<meta http-equiv="refresh" content="0; url=' + url + '">');
            popup.document.close();
        }

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'tumblr',
            section: 'right_column',
            component: 'share_options',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        return (
            <div className="share-icon-wrap">
                <a href="javascript:void(0)" onClick={this.handleClick} className={classnames([
                        'share-icon',
                        'share-tumblr',
                        {'disabled': this.isDisabled()}
                    ])}>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="320px" height="320px" viewBox="0 0 320 320" enable-background="new 0 0 320 320">
                        <path fill="#fff" d="M198.034,216.28c-3.479,1.657-10.134,3.103-15.097,3.232c-14.978,0.402-17.88-10.52-18.008-18.448v-58.246h37.573v-28.327
            h-37.436V66.824c0,0-26.962,0-27.411,0c-0.448,0-1.236,0.398-1.345,1.401c-1.602,14.589-8.433,40.192-36.814,50.423v24.17h18.933
            v61.139c0,20.929,15.445,50.665,56.204,49.97c13.751-0.238,29.022-5.997,32.401-10.959L198.034,216.28z"></path>
                    </svg>
                </a>
            </div>
            );
    }
}

module.exports = TumblrIcon;
