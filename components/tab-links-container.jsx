'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const classnames = require('lib/tp/classnames/classnames');
const TabLink = require('lib/da/peek/components/tab-link');

// Cache these globally so we don't have to do a getBoundingClientRect for every
// slide. The only data that matters here is the `width` of the tab links and
// the tab `left` values relative to the tab links container. Since the right
// side of peek has a fixed width there is no need to worry about window resize
// or anything of that nature. These will only need to be cached once.
let containerRect = null;
let tabRects = {};

class TabLinksContainer extends ReactBase {
    constructor(props) {
        super(props);
        this.state = {
            underlineStyle: {}
        }
    }
    componentDidMount() {
        this.setUnderlineStyles();
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.activeTab !== this.props.activeTab) {
            this.setUnderlineStyles();
        }
    }
    setUnderlineStyles() {
        // if the rects haven't been cached globally yet do it now
        if (!containerRect) {
            containerRect = ReactDOM.findDOMNode(this.refs.tabLinkContainer).getBoundingClientRect();
            this.props.tabs.forEach((tab) => {
                tabRects[tab] = ReactDOM.findDOMNode(this.refs[tab]).getBoundingClientRect();
            });
        }
        this.setState({
            underlineStyle: {
                width: tabRects[this.props.activeTab].width + 'px',
                transform: 'translateX(' + (tabRects[this.props.activeTab].left - containerRect.left) + 'px)'
            }
        });
    }
    render() {
        return (
            <div className="tab-link-container" ref="tabLinkContainer">
                <div className="links-wrap">
                    {this.props.tabs.map(tab => {
                        return (
                            <TabLink
                                key={tab}
                                name={tab}
                                ref={tab}
                                isActive={(this.props.activeTab == tab)}
                                changeActiveTab={this.props.changeActiveTab} />
                            );
                    })}
                </div>
                <div className="underline-wrap">
                    <div className="underline" style={this.state.underlineStyle} />
                </div>
            </div>
        );
    }
}

module.exports = TabLinksContainer;
