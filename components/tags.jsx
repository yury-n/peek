'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const classnames = require('lib/tp/classnames/classnames');

class Tags extends ReactBase {
    constructor(props) {
        super(props);
        this.state = {
            isTagsCollapsible: false
        };
    }
    componentDidMount() {
        this.tagsNode = ReactDOM.findDOMNode(this.refs['tags-content']);
    }
    componentDidUpdate(prevPops, prevState) {
        if (!this.state.isTagsCollapsible && this.tagsNode && this.tagsNode.offsetHeight > 36) {
            this.setState({
                isTagsCollapsible: true
            });
        }
    }
    handleDescriptionCollapserClick() {
        this.props.actions.toggleInfoTagsCollapsed();
    }
    generateToggler() {
        if (this.state.isTagsCollapsible) {
            return (
                <div className="collapser" onClick={ this.handleDescriptionCollapserClick.bind(this) }>{ this.props.tagsCollapsed ? 'MORE' : 'LESS' }</div>
            );
        }
    }
    generateTagList() {
        let tagList = this.props.tags;
        let tags = []; 
        for (let i = 0; i < tagList.length; i++) {
            let tagHref = "http://www.deviantart.com/tag/" + tagList[i];
            tags.push(<a key={ i } href={ tagHref } className="tag">{ tagList[i] }</a>);
        }
        return tags;
    }
    render() {
        return (
            <div ref="tags-content" className={ classnames('tags', {
                collapsed: this.props.tagsCollapsed,
                expanded: !this.props.tagsCollapsed
            }) }>
                { this.generateTagList() }
                { this.generateToggler() }
            </div>
        );
    }
}

module.exports = Tags;