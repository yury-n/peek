'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Loading = require('lib/da/peek/components/loading');
const Hotkeys = require('lib/da/hotkeys');
// const deviations = require('lib/da/api/deviation');

class Image extends ReactBase {
    constructor() { 
        super();
        Hotkeys.register(['z'], this.zoomClick);
    }

    getSrc() {
        let url = this.props.nodeMetadata.src || '';
        return url.replace('http:', '');
    }

    zoomClick() {
        if (this.props.position === 0) {
            this.props.actions.deviationImageClick();
        }
    }

    render() {
        return <div className="image-deviation"><img className="image" src={this.getSrc()} onClick={ this.zoomClick } /></div>;
    }
}

module.exports = Image;
