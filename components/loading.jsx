'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');


class Loading extends ReactBase {
    render() {
        const llama = this.props.llamaless ? null : <div className="llama"><img src="//st.deviantart.net/loading/llama-med.gif" /></div>;
        const text = this.props.text || 'Loading...';
        return (<div className={classnames([ 'loading', this.props.classes ])}>{ llama }{ text }</div>);
    }
}

module.exports = Loading;
