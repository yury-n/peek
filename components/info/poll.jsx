'use strict';

const React = require('lib/tp/react');
const InfoStatus = require('lib/da/peek/components/info/status');
const Share = require('lib/da/peek/components/share');

class InfoPoll extends InfoStatus {
    generateShare() {
        return (<Share
            id={ this.props.nodeMetadata.id }
            author={ this.props.author }
            isOpen={ this.props.shareOpen }
            type={ this.props.type }
            sizing={ this.props.nodeMetadata.sizing }
            url={ this.props.url }
            title={ this.props.title }
            mature={ this.props.nodeMetadata.mature }
            description={ this.props.truncatedDescription }
            triangleLeft={ this.getTriangleLeft() }
            disableStatusShare={ true }
        />);
    }
}

module.exports = InfoPoll;
