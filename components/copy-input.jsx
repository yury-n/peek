'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');

class CopyInput extends ReactBase {
    handleClick(e) {
        e.preventDefault();
        ReactDOM.findDOMNode(this.refs.input).select();
    }
    render() {
        return (
            <div className="copy-input" onClick={ this.handleClick }>
                <label><b>{ this.props.label }</b><input ref="input" type="text" readOnly="1" value={ this.props.value } /></label>
            </div>
        )
    }
}

module.exports = CopyInput;
