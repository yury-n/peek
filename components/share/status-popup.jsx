'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Alt = require('lib/tp/alt');
const SharePopupActions = require('lib/da/peek/actions/share-popup');
const StatusPopupActions = require('lib/da/peek/actions/status-popup');
const StatusPopupStore = require('lib/da/peek/stores/status-popup');
const Loading = require('lib/da/peek/components/loading');

const deviationCache = require('lib/da/cache/deviation');
const statusCache = require('lib/da/cache/status');

const SharePopupContainer = require('lib/da/peek/components/share/popup-container');
const ShareInfo = require('lib/da/peek/components/share/info');
const CloseX = require('lib/da/peek/components/share/close-x');

class StatusPopup extends ReactBase {
    constructor(props) {
        super(props);
        this.alt = new Alt();
        this.alt.addActions('SharePopupActions', SharePopupActions);
        this.alt.addActions('StatusPopupActions', StatusPopupActions);
        this.alt.addStore('StatusPopupStore', StatusPopupStore);
        this.sharePopupActions = this.alt.getActions('SharePopupActions');
        this.statusPopupActions = this.alt.getActions('StatusPopupActions');
        this.statusPopupStore = this.alt.getStore('StatusPopupStore');

        this.state = this.statusPopupStore.getState();
    }
    componentDidMount() {
        this.statusPopupStore.listen(this.onChange);

        switch (this.props.type) {
            case 'status':
                this.sharePopupActions.fetchStatusContent(this.props.id, this.props.author.userid);
                break;
            case 'collection':
                this.sharePopupActions.setCollectionData({author: this.props.author, collection: this.props.collection});
                break;
            default:
                this.sharePopupActions.fetchDeviationMetadata(this.props.id);
                break;
        }
    }
    componentWillUnmount() {
        this.statusPopupStore.unlisten(this.onChange);
    }
    onChange() {  
        this.setState(this.statusPopupStore.getState());
    }
    // callback for "Status" textarea onChange event
    handleStatusChange(e) {
        this.statusPopupActions.setStatusValue(e.target.value);
    }
    // resets the form then closes the share popup
    clearAndClose() {
        this.sharePopupActions.resetForm();
        this.props.hide();
    }
    // Submit button is disabled until we have enough data to try and send.
    // Params for dapi are already determined by the store's fetch callbacks.
    postStatus() {
        let body;
        if (this.state.collectionUrl) {
            body = this.state.status + '\n\n' + this.state.collectionUrl
        } else {
            body = this.state.status
        }

        this.statusPopupActions.postStatus(
            this.state.attachmentType,
            this.state.sharedItemId,
            this.state.parentId,
            body
        );
    }
    // display correct sub heading based on the item being shared
    getSubHeadingText() {
        if (this.props.isStatus && !this.state.overrideIsStatusProp) {
            return 'Share this status with your watchers';
        } else {
            return 'Share this deviation with your watchers';
        }
    }
    // renders basic info about the item being shared
    getInfo() {
        return (
            <ShareInfo
                thumbSrc={this.props.thumbSrc}
                username={this.state.author.username}
                usericon={this.state.author.usericon}
                mature={ this.props.mature }
                title={this.state.title} />
            );
    }
    // renders the feedback message in state if set, otherwise renders the
    // cancel/submit buttons
    getButtonsOrFeedback() {
        if (!this.state.loaded) {
            return <Loading />;
        }

        if (this.state.feedback) {
            let reset = null;
            if (this.state.showResetWithFeedback) {
                reset = <a href="javascript:void(0)" onClick={this.sharePopupActions.resetForm}>Reset Form</a>;
            }
            return (
                <p className="feedback">
                    {this.state.feedback + ' '}
                    {reset}
                </p>
                );
        } else {
            const disabled = !this.state.loaded || this.state.unshareable;
            return (
                <div className="buttons-wrap">
                    <button className="btn cancel" onClick={this.clearAndClose}>Cancel</button>
                    <button className="btn primary submit" onClick={this.postStatus} disabled={disabled}>Post Status</button>
                </div>
                );
        }
    }
    render() {
        return (
            <SharePopupContainer hide={this.clearAndClose}>
                <div className="heading">Share To Your Watchers</div>
                <div className="sub-heading">{this.getSubHeadingText()}</div>
                {this.getInfo()}
                <textarea ref="message" placeholder="Write something about this!" value={this.state.status} onChange={this.handleStatusChange} />
                {this.getButtonsOrFeedback()}
                <CloseX onClick={this.clearAndClose} />
            </SharePopupContainer>
            );
    }
}

module.exports = StatusPopup;
