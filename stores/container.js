'use strict';

const sigil = require('lib/dom/sigil');
const Meta = require('lib/data/meta');
const dapx = require('lib/da/dapx');

const minOffset = -2;
const maxOffset = 2;
const startingPosition = maxOffset + 1;
const navOffset = 82;

const DIRECTION_LEFT = 'left';
const DIRECTION_RIGHT = 'right';

class Store {
    constructor() {
        this.bindActions(this.alt.getActions('PeekContainerActions'));

        this.meta = Meta.createLocal('container_Store');
        this.resetData();

        this.paddles = {
            left: {
                visible: true
            },
            right: {
                visible: true
            },
            loading: {
                visible: false
            }
        }
    }
    resetData() {
        this.nextId = 0;

        this.slideable = false;
        this.slides = {};
        this.activeSlideId = null;

        this.tailSlideId = null;
        this.headSlideId = null;

        this.fetching = false;
        this.fetchNoMore = false;

        this.distanceFromOpen = 0;
        this.slidingDirection = DIRECTION_RIGHT;
    }
    onUpdateNodes(data) {
        const nodes = sigil.find(data.wrapper, data.sigil);
        const newSlides = [];

        if (data.reset) {
            this.resetData();
        }

        for (let i = 0; i < nodes.length; i++) {
            const data = this.meta.get(nodes[i]);

            // only process it if we haven't initialized it yet
            if (data.id === undefined) {
                const id = this.nextId++;

                const slide = {
                    id: id,
                    // if we're past the first node
                    left: (this.hasHead()) ? id - 1 : null,
                    // if we're not at the last node yet
                    right: (i < nodes.length - 1) ? id + 1 : null,
                    // far right
                    position: startingPosition,
                    // pass data directly from the node
                    nodeMetadata: Meta.getGlobal(nodes[i]),
                    // a reference to where we might want to scroll/ramjet back to
                    node: nodes[i]
                };
                newSlides.push(slide);

                // set the local id so we know that we've touched it
                this.meta.set(nodes[i], {id: id});

                // this assumes we'll never append things to the front of the slides
                if (!this.hasHead()) {
                    this.headSlideId = id;
                }
            }
        }

        this.setPaddle('loading', false);
        if (!newSlides.length) {
            // can't go any farther
            this.setPaddle('right', false);

            // don't change this.fetching so we prevents any other fetches
            this.fetchNoMore = true;
            return;
        }
        this.setPaddle('right', true);

        if (newSlides.length === 1) {
            this.setPaddle('right', false);
        }

        const firstSlide = newSlides[0];

        // update the first, last, and active ids slides
        if (this.hasTail()) {
            this.slides[this.tailSlideId].right = firstSlide.id;
            firstSlide.left = this.tailSlideId;
        }
        this.tailSlideId = newSlides[newSlides.length - 1].id;

        for (let i = 0; i < newSlides.length; i++) {
            this.slides[newSlides[i].id] = newSlides[i];
        }

        // update slides only if we're inserting them into an already-open peek
        if (this.fetching) {
            this.positionSlides();
        }

        this.fetching = false;
    }
    onOpenAt(node) {
        this.distanceFromOpen = 0;
        const data = this.meta.get(node);

        // if we were opened at one point, but have now opened at a different starting place
        if (this.activeSlideId !== null && data.id != this.activeSlideId) {
            // figure out in which direction it is in relation to the current id
            if (this.isRightOf(data.id, this.activeSlideId)) {
                this.setPositionUntil('right', data.id, this.currentSlide().right, maxOffset);
            } else {
                this.setPositionUntil('left', data.id, this.currentSlide().left, minOffset);
            }
        }

        this.activeSlideId = data.id;
        this.positionSlides();

        // set paddles
        const activeSlide = this.slides[this.activeSlideId];
        if (activeSlide.left) {
            this.setPaddle('left', true);
        } else {
            this.setPaddle('left', false);
        }
        if (activeSlide.right) {
            this.setPaddle('right', true);
        } else {
            this.setPaddle('right', false);
        }

        // We give the modal a chance to open before we allow the sides to 
        // be able to use the CSS transitions, otherwise on subsequent opens
        // the slides will transition to the place instead of just opening 
        // at the correct slide
        setTimeout(() => {
            if (!this.slideable) {
                this.slideable = true;
                this.emitChange();
            }
        }, 300);

    }
    
    onSlide() {
        const youtubes = sigil.find('embedded-youtube');
        for (let i = 0; i < youtubes.length; i++) {
            let frame = youtubes[i];
            frame.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        }
    }

    onSlideRight() {
        if (this.hasRight()) {
            this.onSlide();
            this.goingRight(true);

            this.activeSlideId = this.currentSlide().right;
            this.positionSlides();

            // if the new current slide doesn't have anythign to the right,
            // preemptively fetch the next slide(s)
            if (!this.hasRight()) {
                if (!this.fetching) {
                    this.getInstance().getEventEmitter().emit('fetch');
                    this.fetching = true;
                }
                this.setPaddle('right', false);
                if (!this.fetchNoMore) {
                    this.setPaddle('loading', true);
                }
            }
        }
    }
    onSlideLeft() {
        if (this.hasLeft()) {
            this.onSlide();
            this.goingRight(false);

            this.setPaddle('right', true);
            this.setPaddle('loading', false);

            this.activeSlideId = this.currentSlide().left;
            this.positionSlides();
        }
    }
    onUnslideable() {
        this.slideable = false;
    }
    onScrollToLast() {
        // we make the assumption that we're on a page that has nav
        let last = this.lastSeenSlide();
        let scrollY = window.scrollY || window.pageYOffset;
        // use the most recently seen node as the one to scroll to
        window.scrollTo(0, scrollY + last.node.getBoundingClientRect().top - navOffset);
    }
    /**
     * Update the slides two away from the active slide (indlucing the active slide)
     * so that they are in the correct position
     * we must assume no sequential ids, so can't just add the offset to the activeSlideId
     */
    positionSlides() {
        this.currentSlide().position = 0;

        this.setPaddle('left', this.hasLeft());
        if (this.fetchNoMore && !this.hasRight()) {
            this.setPaddle('right', false);
        }

        // going backwards
        this.setPositionFor(this.currentSlide().left, 'left', startingPosition, true);

        // going forwards
        this.setPositionFor(this.currentSlide().right, 'right', startingPosition, false);
    }
    currentSlide() {
        return this.slides[this.activeSlideId];
    }
    lastSeenSlide() {
        return this.currentSlide();
    }
    setPaddle(paddle, visible) {
        this.paddles[paddle].visible = visible;
    }
    hasTail() {
        return this.tailSlideId !== null;
    }
    hasHead() {
        return this.headSlideId !== null;
    }
    hasLeft() {
        return this.currentSlide().left !== null;
    }
    hasRight() {
        return this.currentSlide().right !== null;
    }
    isRightOf(start, goUntil) {
        let slide = this.slides[start];

        while (slide.right !== null) {
            if (slide.right == goUntil) {
                return true;
            }
            slide = this.slides[slide.right];
        }

        return false;
    }
    setPositionFor(start, direction, max, negate) {
        let slide = this.slides[start];
        let reached = 1;

        while (slide && reached < max) {
            slide.position = (negate) ? reached * -1 : reached;
            slide = this.slides[slide[direction]];
            reached++;
        }
    }
    setPositionUntil(direction, start, goUntil, position) {
        if (goUntil && this.slides[goUntil]) {
            this.slides[goUntil].position = position;
        }

        let slide = this.slides[this.slides[start][direction]];

        while (slide && slide.id !== goUntil) {
            slide.position = position;
            slide = this.slides[slide[direction]];
        }
    }
    goingRight(set) {
        if (set === undefined) {
            return this.slidingDirection === DIRECTION_RIGHT;
        }
        this.slidingDirection = (set) ? DIRECTION_RIGHT : DIRECTION_LEFT;
    }
    onLogTakeoverClick() {
        const slide = this.currentSlide();
        const widget = sigil.closest(slide.node, 'tab-container-gallerywidget');

        const extraData = {
            itemtype: slide.nodeMetadata.type,
            itemid: slide.nodeMetadata.id,
            itemowner: slide.nodeMetadata.author.userid,
            // cry.jpg
            context: widget ? Meta.getGlobal(widget).campaignJournalId : 'gallerywidget'
        }

        // override itemowner if there is a curator set
        if (slide.nodeMetadata.curator) {
            extraData.itemowner = slide.nodeMetadata.curator.userid;
        }

        dapx.sendStandardizedPayload({
            action: 'click',
            view: 'peek',
            element: 'background_image',
        }, extraData);
    }
}

module.exports = Store;
