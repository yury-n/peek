'use strict';

const mature = require('lib/da/mature');
const { React, ReactBase } = require('lib/tp/react-base');

class ShareInfo extends ReactBase {
    generateThumb() {
        if (this.props.mature && !mature.visible()) {
            return <div className="img-wrap">
                        <div className="blocked-content">
                            <div className="mature-state-msg">
                                MATURE CONTENT
                            </div>
                        </div>
                    </div>
        }
        if (this.props.thumbSrc) {
            return <div className="img-wrap"><img src={this.props.thumbSrc.replace('http:', '')} /></div>;
        }
    }
    generateUsericon() {
        if (this.props.usericon) {
            return <img className="usericon" src={this.props.usericon} />;
        }
    }
    render() {
        return (
            <div className="info">
                { this.generateThumb() }
                <div className="details">
                    <div className="title">{this.props.title}</div>
                    { this.generateUsericon() }
                    <span className="username">{this.props.username}</span>
                </div>
            </div>
            );
    }
}

module.exports = ShareInfo;
