'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const mature = require('lib/da/mature');
const types = {
    mature: require('lib/da/peek/components/types/mature'),
    image: require('lib/da/peek/components/types/image'),
    freeform: require('lib/da/peek/components/types/freeform'),
    flash: require('lib/da/peek/components/types/flash'),
    madefire: require('lib/da/peek/components/types/madefire')
}

class Deviation extends ReactBase {
    handleTakeoverClick() {
        this.props.takeover.log();
    }

    generateHeader() {
        if (this.props.header) {
            return this.props.header;
        }

        // otherwise we have a "takeover"
        if (this.props.takeover && this.props.takeover.src) {
            return (
                <a data-sigil="peek-noclose" href={ this.props.takeover.url } target="_blank" onClick={ this.handleTakeoverClick } >
                    <img src={ this.props.takeover.src } />
                </a>
            );
        }

        return false;
    }

    render() {
        const type = mature.visible() || !this.props.nodeMetadata.mature ? this.props.nodeMetadata.type : 'mature';
        const Deviation = types[type];

        return (
            <div>
                { this.generateHeader() }
                <Deviation {...this.props} />
            </div>
        );
    }
}

module.exports = Deviation;

