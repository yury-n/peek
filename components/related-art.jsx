'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class RelatedArt extends ReactBase {
    getMoreMarkup(html) {
        return (<div className="related-art-artist torpedo-container" ref="related-art-artist-container" dangerouslySetInnerHTML={{__html: html }}></div>);
    }

    generateMLT() {
        if (this.props.mlt) {
            return this.getMoreMarkup(this.props.mlt);
        }
        return false;
    }

    generateMFA() {
        if (this.props.mfa) {
            return this.getMoreMarkup(this.props.mfa);
        }
        return false;
    }

    generateViewMore() {
        if (this.props.viewMore) {
            return (<a key="btn" href={ 'http://' + this.props.username + '.deviantart.com' } className="view-more btn dark good">View More</a>);
        }
    }

    render() {
        return (<div>
                    <h2>
                        <a href={ 'http://' + this.props.username + '.deviantart.com'} target="_blank">
                            More from <strong>{this.props.username}</strong> <i className="icon-arrow"></i>
                        </a>
                    </h2>
                    { this.generateMLT() }
                    { this.generateMFA() }
                    { this.generateViewMore() }
                </div>);
    }
}

module.exports = RelatedArt;
