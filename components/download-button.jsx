'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class DownloadButton extends ReactBase {
    downloadClick() {
        if (this.props.position === 0) {
            // make a fake a tag that opens in a new tab
            const a = document.createElement('a');
            a.setAttribute('href', this.props.downloadUrl);
            a.setAttribute('target', '_blank');
            a.click();

            // log event T26830
            dapx.sendStandardizedPayload({
                view: 'peek',
                element: 'download_btn',
                section: 'right_column',
                component: 'buttons_block',
                action: 'click'
            });
        }
    }
    render() {
        return (
            <div className={classnames([
                'download-button',
                'btn',
                'dark',
                'good',
            ])} onClick={this.downloadClick} title="Download">
                <div className="icon-download-simple" />
            </div>
            )
    }
}

module.exports = DownloadButton;

