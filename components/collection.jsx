'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const Deviation = require('lib/da/peek/components/deviation');
const UserLink = require('lib/da/user/components/user-link');
const classnames = require('lib/tp/classnames/classnames');
const Meta = require('lib/data/meta');
const Torpedo = require('lib/da/torpedo/torpedo');
const sigil = require('lib/dom/sigil');
const mature = require('lib/da/mature');
const Hotkeys = require('lib/da/hotkeys');

class Collection extends ReactBase {
    constructor(props) {
        super(props);

        this.peek = false;
        this.peekActive = false;
        this.header = false;
        
        Hotkeys.register(['m'], this.showMore);

    }

    showMore() {
        if (this.props.position === 0) {
            this.props.actions.toggleCollectionExpanded();
        }
    }
    
    componentDidMount() {
        this.props.actions.fetchCollectionGrid(
            this.props.nodeMetadata.collection_data.gallection_meta.gallectionid,
            this.props.nodeMetadata.peek_deviationids
        );
    }

    componentWillUnmount() {
        // delete nested peek
        if (this.peek) {
            this.peek.delete();
        }
        // deactivate torpedo
        if (this.torpedo) {
            this.torpedo.deactivate();
        }
    }

    getContainer() {
        return ReactDOM.findDOMNode(this.refs.torpedoContainer);
    }

    componentDidUpdate() {
        const container = this.getContainer();

        // generate torpedo grid once slide content and torpedo html has loaded
        if (this.props.gridLoaded && this.props.contentLoaded && !this.torpedo) {
            Meta.setContainerTorpedoName(container, 'peek-torpedo');
            this.torpedo = new Torpedo(container);
            this.torpedo.generate();
        }

        // create peek instance when in the center slide position and torpedo
        // has generated
        if (this.props.position === 0 && this.torpedo && !this.peek) {
            const slideProps = {
                header: this.header,
                isNested: true,
                context: 'collection'
            };
            this.peek = this.props.peek(container, 'peekable', slideProps);
        }

        if (this.props.gridLoaded) {
            const thumbGrid = sigil.find(container, 'torpedo-thumb');
            const extraHeight = 160; // height of the header and padding at the bottom of the container
            if (!this.props.expanded) {
                let blockHeight = 0;
                for (let i = 0; i < thumbGrid.length; i++) {
                    let thumb = thumbGrid[i];
                    let meta = Meta.getGlobal(thumb);
                    if (meta.row == 1) {
                        // at this point the thumb is the first in the second row, so use the ofset height of it.
                        blockHeight = (thumb.offsetTop + thumb.offsetHeight) - extraHeight;
                        break;
                    }
                }
                if (container.offsetHeight < blockHeight || blockHeight == 0) {
                   this.hideExtra = true; 
                } else {
                    container.style.maxHeight = blockHeight + 'px';
                }
            } else {
                container.style.maxHeight = '10000px';
            }
        }
    }

    activateTorpedo(evt) {
        /***
         * This needs to be seperate because react updates the refs after componentDidUpdate.
         * If this is called when setting up peek things go horribly wrong and it all falls over,
         * so it gets split out to here to be called seperately.
         */
        if (this.peek && !this.peekActive) {
            this.peek.updateNodes();
            this.peekActive = true;

            this.peek.on('fetch', () => {
                window.requestAnimationFrame(() => {
                    this.peek.updateNodes();
                });
            });
        }
    }

    hide() {
        if (this.peek) {
            this.peek.hide();
        }
    }

    generateThumbgrid() {
        const html = this.props.deviations.map((deviation) => {
            return deviation.html;
        });
        return <div ref="torpedoContainer" dangerouslySetInnerHTML={{__html: html.join('')}} />;
    }

    render() {
        if (!this.props.gridLoaded) {
            return null;
        }
 
        const style = {
            backgroundImage: !mature.visible() && this.props.nodeMetadata.mature ? '' : 'url(' + this.props.nodeMetadata.src + ')',
        }
        /***
         * Need to keep the header around to put it on child peeks so we don't have to re-fetch and compile it every time.
         */
        this.header = (<div className="header">
            <div className="cover" style={ style } />
            <div className="controls">
                <span className="back" onClick={ this.hide }>Back</span> <a className="view" href={ this.props.url }>View Collection</a>
            </div>
            <div className="details">
                <div className="title">{ this.props.nodeMetadata.collection_data.gallection_meta.name }</div>
                <div className="by">Curated by <UserLink username={ this.props.nodeMetadata.curator.username } /></div>
            </div>
        </div>)

        return (
            <div className="collection" >
                { this.header }
                <div className="collection-torpedo torpedo-container torpedo-container-xs show-orphans" onMouseEnter={ this.activateTorpedo }>
                    { this.generateThumbgrid() }
                </div>
                <div className={ classnames(['collection-expander', {'hidden' : ( this.props.expanded || this.hideExtra )}])} onClick={ this.showMore }>Show More</div>
            </div>
        );
    }
}

module.exports = Collection;
