'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
class Mature extends ReactBase {
    render() {
        return <div className="blocked-content">
                     <div className="mature-state-msg">MATURE CONTENT</div>
                </div>
    }
}

module.exports = Mature;

