'use strict';

class Actions {
    constructor() {
        this.generateActions('openAt', 'updateNodes', 'slideRight', 'slideLeft', 'fetchMore', 'unslideable', 'scrollToLast', 'logTakeoverClick');
    }
}

module.exports = Actions;