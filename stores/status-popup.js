'use strict';

class StatusPopupStore {
    constructor() {
        this.bindActions(this.alt.getActions('SharePopupActions'));
        this.bindActions(this.alt.getActions('StatusPopupActions'));

        this.status = '';
        this.loaded = false;
        this.feedback = null;
        this.author = {};
        this.unshareable = false;
    }
    onFetchStatusContent(data) {
        // Set the `sharedItemId` and `attachmentType` appropriately
        if (data.is_share) {
            if (data.items && data.items[0]) {
                switch (data.items[0].type) {
                    case 'deviation':
                        if (!data.items[0].deviation.is_deleted) {
                            // In the case of statuses with an attached
                            // deviation we share the deviation and mark the
                            // status as the parent. Then set the deviation
                            // author and title to state so the info we display
                            // about the item you're actually sharing is
                            // correct.
                            this.sharedItemId = data.items[0].deviation.deviationid;
                            this.attachmentType = 'deviation';
                            this.parentId = data.author.username + ':' + data.statusid.id;
                            this.author = data.items[0].deviation.author;
                            this.title = data.items[0].deviation.title;
                            this.loaded = true;
                            this.overrideIsStatusProp = true;
                            return;
                        }
                        break;
                    case 'status':
                        if (!data.items[0].status.is_deleted) {
                            // In the case of statuses with an attached status
                            // we share the attached status and mark the other
                            // as the parent. Set the attached status's author
                            // to state so the info shown about the item
                            // actually being shared is correct.
                            this.sharedItemId = data.items[0].status.author.username + ':' + data.items[0].status.statusid.id;
                            this.attachmentType = 'status';
                            this.parentId = data.author.username + ':' + data.statusid.id;
                            this.author = data.items[0].status.author;
                            this.loaded = true;
                            return;
                        }
                        break;
                }
            }
            // if we got here something ain't right
            this.feedback = 'Sorry, this status cannot be shared.';
            this.showResetWithFeedback = false;
            this.unshareable = true;
        } else {
            this.sharedItemId = data.author.username + ':' + data.statusid.id;
            this.attachmentType = 'status';
            this.author = data.author;
            this.loaded = true;
        }
    }
    onFetchDeviationMetadata(data) {
        if (!data) {
            this.feedback = 'Sorry, this deviation cannot be shared.';
            this.showResetWithFeedback = false;
            this.unshareable = true;
        } else {
            this.title = data.metadata[0].title;
            this.author = data.metadata[0].author;
            this.attachmentType = 'deviation';
            this.sharedItemId = data.metadata[0].deviationid;
        }
        
        this.loaded = true;
    }
    onSetCollectionData(data) {
        this.author = data.author;
        this.title = data.collection.gallection_meta.name;
        this.subject = this.title + ' by ' + this.author.username;

        const baseUrl = 'http://' + this.author.username + '.deviantart.com/favourites/';

        // we can't directly link to featured even though it has a unique id
        if (this.title == 'Featured') {
            this.collectionUrl = baseUrl;
        } else {
            this.collectionUrl = baseUrl + data.collection.gallection_meta.gallectionid;
        }

        this.loaded = true;
    }
    onSetStatusValue(value) {
        this.status = value;
    }
    onResetForm() {
        this.status = '';
        this.feedback = null;
    }
    onPostStatus() {
        this.feedback = 'Posting status...';
        this.showResetWithFeedback = false;
    }
    onPostStatusDone(feedback) {
        this.feedback = feedback;
        this.showResetWithFeedback = true;
    }
}

module.exports = StatusPopupStore;
