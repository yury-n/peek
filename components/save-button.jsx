'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const Hotkeys = require('lib/da/hotkeys');
const dapx = require('lib/da/dapx');

class SaveButton extends ReactBase {
    handleClick() {
        if (!this.props.disabled && this.props.position === 0) {

            // is the item currently saved?
            if (this.props.isSaved) {
                // then unsave
                this.props.unsaveDeviation(this.props.deviationid);
            } else {
                // save
                this.props.saveDeviation(this.props.deviationid);
            }

            // log event T26830
            dapx.sendStandardizedPayload({
                view: 'peek',
                element: 'save_btn',
                section: 'right_column',
                component: 'buttons_block',
                action: 'click'
            });
        }
    }
    render() {
        Hotkeys.register(['s'], this.handleClick);
        return (
            <div className={classnames([
                'btn',
                'dark',
                'good',
                {'disabled': this.props.disabled},
                {'saved': this.props.isSaved}
            ])} data-sigil="peek-save-button" onClick={this.handleClick} title="Save">
                <div className="clock" />
            </div>
            )
    }
}

module.exports = SaveButton;

