'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class TwitterIcon extends ReactBase {
    handleClick() {
        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'twitter',
            section: 'right_column',
            component: 'share_options',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        let disabled = true;
        let url = 'javascript:void(0)';

        if (this.props.url) {
            disabled = false;
            let name = this.props.author.username;
            if (this.props.twitterHandle) {
                // TODO: implement this?
                name = '@' + this.props.twitterHandle;
            }

            const tweet = this.props.title + ' by ' + name;

            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(tweet);
            url += '&url=' + encodeURIComponent(this.props.url);
            url += '&related=deviantart';
        }

        return (
            <div className="share-icon-wrap">
                <a onClick={this.handleClick} href={url} target="_blank" className={classnames([
                        'share-icon',
                        'share-twitter',
                        {'disabled': disabled}
                    ])}>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="300px" height="300px" viewBox="0 0 300 300" enable-background="new 0 0 300 300">
                        <g>
                            <path fill="#fff" d="M242.021,95.014c-6.544,2.902-13.576,4.864-20.958,5.746c7.534-4.516,13.32-11.667,16.044-20.188
            c-7.05,4.182-14.86,7.219-23.172,8.855c-6.656-7.092-16.14-11.523-26.635-11.523c-20.153,0-36.491,16.338-36.491,36.489
            c0,2.86,0.323,5.645,0.945,8.316c-30.327-1.522-57.214-16.049-75.211-38.126c-3.141,5.389-4.94,11.658-4.94,18.345
            c0,12.659,6.442,23.828,16.233,30.372c-5.982-0.189-11.608-1.831-16.528-4.564c-0.003,0.152-0.003,0.305-0.003,0.459
            c0,17.68,12.578,32.428,29.271,35.78c-3.062,0.834-6.286,1.28-9.614,1.28c-2.351,0-4.637-0.229-6.865-0.654
            c4.644,14.497,18.119,25.047,34.087,25.342c-12.489,9.786-28.222,15.62-45.318,15.62c-2.945,0-5.85-0.173-8.705-0.51
            c16.149,10.353,35.329,16.394,55.936,16.394c67.119,0,103.821-55.602,103.821-103.822c0-1.582-0.036-3.156-0.105-4.72
            C230.942,108.759,237.129,102.332,242.021,95.014z"></path>
                        </g>
                    </svg>
                </a>
            </div>
            );
    }
}

module.exports = TwitterIcon;
