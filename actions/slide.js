'use strict';

const Promise = require('lib/promise').Promise;
const deviation = require('lib/da/cache/deviation');
const comments = require('lib/da/comments');
const gallection = require('lib/da/gallection');
const Toast = require('lib/da/toast');
const FaveToast = require('lib/da/toast/components/fave');
const SaveToast = require('lib/da/toast/components/save');
const WatchToast = require('lib/da/toast/components/watch');
const CollectToast = require('lib/da/toast/components/collect');
const DeviationTypes = require('lib/da/peek/constants/deviation-types');
const status = require('lib/da/cache/status');
const poll = require('lib/da/poll');
const sigil = require('lib/dom/sigil');
const Meta = require('lib/data/meta');
const dapi = require('lib/da/dapi');
const EE = require('lib/data/emit');

const PEEK_TORPEDO_NAME = 'peek-torpedo';

class Actions {
    constructor() {
        this.generateActions(
            'fetchContentDone',
            'deviationImageClick',
            'fetchRelatedArtDone',
            'fetchCollectionGridDone',
            'moreCommentsClick',
            'setActiveInfoTab',
            'paginateClickDone',
            'watchChangeDone',
            'faveClickDone',
            'slideMount',
            'slideUnmount',
            'fetchStatusAttachmentDone',
            'toggleShareOpen',
            'toggleInfoDescriptionCollapsed',
            'toggleInfoTagsCollapsed',
            'saveDeviationDone',
            'unsaveDeviationDone',
            'saveError',
            'toggleCollectionExpanded',
            'voteDone',
            'toggleCollectionExpanded',
            'faveDropdownSelectDone',
            'faveDropdownFetchDone',
            'faveNewCollectionSubmitDone',
            'faveNewCollectionChange',
            'faveNewCollectionError',
            'setTrackingPixelHTML',
            'forceState'
        );
    }

    fetchContent(data) {
        this.dispatch(data);
        let metadataPromise, commentPromise, deviationPromise, statusPromise, collectionPromise, pollPromise;
        const promisesArray = [];
        const returnIndex = [];

        switch (data.type) {
            case 'collection':
                commentPromise = comments.getCollection(data.collection_data.gallection_meta.gallectionid, 0);
                break;
            case 'status':
                commentPromise = comments.getStatus(data.id, 0);
                break;
            case 'poll':
                commentPromise = comments.getPoll(data.id, 0);
                break;
            default:
                if (DeviationTypes.test(data.type)) {
                    commentPromise = comments.getDeviation(data.id, 0);
                }
                break;
        }

        if (commentPromise) {
            promisesArray.push(commentPromise);
            returnIndex.push('comments');
        }

        if (data.type == 'freeform') {
            deviationPromise = deviation.content(data.id);
            promisesArray.push(deviationPromise);
            returnIndex.push('freeform');
        }

        if (data.type == 'status') {
            statusPromise = status.content(data.id, data.author.userid);
            promisesArray.push(statusPromise);
            returnIndex.push('status');

            // this is only to do more work if the status is a share
            // because the initial response doesn't give us any useful info
            // about the actual share, we need to make an extra request to go get
            // information about it
            statusPromise.then((response) => {
                // we only need to make the extra request if the original status is a share
                if (response.is_share) {
                    const item = response.items[0];
                    status.attachment(data.id, data.author.userid).then((response) => {
                        const attachmentData = {
                            type: response.type
                        };

                        if (response.type == 'status') {
                            attachmentData.status = response.status
                        } else {
                            attachmentData.html = response.html
                        }

                        this.actions.fetchStatusAttachmentDone(attachmentData);
                    }, (error) => {
                        // the api returns a 404 error status for this, so we need to handle it separately
                        this.actions.fetchStatusAttachmentDone({type: 'error'});
                    });
                }
            });
        } else if (data.type == 'collection') {
            // we don't need to actually fetch any data, but we want to wait until all the 
            // other promises return to aggregate all the data
            collectionPromise = new Promise((resolve) => {
                resolve(data);
            });
            promisesArray.push(collectionPromise);
            returnIndex.push('collection');
        } else if (data.type == 'poll') {
            pollPromise = poll.content(data.id, data.userid);
            promisesArray.push(pollPromise);
            returnIndex.push('poll');
        } else if (DeviationTypes.test(data.type)) {
            metadataPromise = deviation.metadata(data.id);
            promisesArray.push(metadataPromise);
            returnIndex.push('metadata');
        }

        Promise.all(promisesArray).then((response) => {
            let returnArray = [];

            for (let i = 0; i < returnIndex.length; i++) {
                returnArray[returnIndex[i]] = response[i];
            }

            this.actions.fetchContentDone(returnArray);
        }).catch((e)=>{console.log('error', e)});
    }

    paginateClick(type, id, offset) {
        this.dispatch(offset);
        switch (type) {
            case 'status' :
                comments.getStatus(id, offset).then((data) => {
                    this.actions.paginateClickDone(data);
                });
                break
            case 'collection' :
                comments.getCollection(id, offset).then((data) => {
                    this.actions.paginateClickDone(data);
                });
                break;
            case 'poll' :
                comments.getPoll(id, offset).then((data) => {
                    this.actions.paginateClickDone(data);
                });
                break;
            default: 
                comments.getDeviation(id, offset).then((data) => {
                    this.actions.paginateClickDone(data);
                });
                break;
        }
    }

    logDeviationPageView(data) {
        if (DeviationTypes.test(data.type)) {
            dapi.request('/deviation/stats', {deviationid: data.id});
        }
    }

    fetchRelatedArt(type, user, id) {
        this.dispatch();

        if (type == 'status' || type == 'poll') {
            dapi.request('/status/statuses', {userid: user}).then((data) => {
                const html = data.related.map((result) => {
                    return result.html;
                });
                this.actions.fetchMoreFromArtistDone(html.join(''));
            });
        } else {
            dapi.request('/browse/morelikethis/preview', {seed: id}).then((data) => {
                const relatedArt = {mfa: null, mlt: null};
                relatedArt.mfa = data.more_from_artist.map((result) => {
                    return result.html;
                }).join('');
                relatedArt.mlt = data.more_from_da.map((result) => {
                    return result.html;
                }).join('');
                this.actions.fetchRelatedArtDone(relatedArt);
            });
        }

        dapi.send();
    }

    watchUser(username) {
        this.dispatch();
        dapi.request('/watch/add', {username : username}).then((data) => {
            const toastProps= { username: username, added : true }
            Toast.displayComponent(WatchToast, toastProps);
            this.actions.watchChangeDone(data);
        });
        dapi.send();
    }

    unwatchUser(username) {
        this.dispatch();
        dapi.request('/watch/remove', {username : username}).then((data) => {
            const toastProps= { username: username, rewatch: this.actions.watchUser, added : false }
            Toast.displayComponent(WatchToast, toastProps);
            this.actions.watchChangeDone(data);
        });
        dapi.send();
    }

    faveClick(faved, id, username, wasSaved) {
        this.dispatch();
        let fave;
        if (faved) {
            fave = gallection.unfave(id);
        } else {
            fave = gallection.fave(id, wasSaved);
        }
        fave.then(() => {
            let toastProps = {faved : faved, id: id, username: username, refave: this.actions.faveClick };
            Toast.displayComponent(FaveToast, toastProps);
            this.actions.faveClickDone();
        });
    }

    faveDropdownClick(loaded) {
        this.dispatch();

        // check to see if it's been loaded already
        if (!loaded) {
            gallection.folders('favourites')
                .then((data) => {
                    this.actions.faveDropdownFetchDone(data.results);
                })
                .catch(() => {
                    this.actions.faveShowError("An error occurred when fetching your collection folders.");
                });
        }
    }

    faveDropdownSelect(payload) {
        // check if it's a valid collection
        if (payload.collectionid === 0) {
            this.actions.faveShowError("Collection is being created. Please wait.");
            return;
        }

        this.dispatch(payload);
        gallection.collect(payload.id, payload.folderid)
            .then(() => {
                this.actions.faveDropdownSelectDone();
                Toast.displayComponent(CollectToast, payload);
            })
            .catch(() => {
                this.actions.faveShowError("An error occurred when saving to your collection folder.");
            });
    }

    faveNewCollectionSubmit(name, description, tags, antisocial) {
        if (!name) {
            this.actions.faveShowError("Collection name is required.");
            return;
        }
        this.dispatch({name: name});
        gallection.create('favourites', name, description, tags, antisocial)
            .then((data) => {
                this.actions.faveNewCollectionSubmitDone(data);
            }).catch((e) => {
                this.actions.faveShowError(e.error_description || "An error occurred");
                this.actions.faveNewCollectionError();
            });
    }

    faveShowError(msg) {
        this.dispatch({msg: msg});
        Toast.displayErrorMessage(msg);
    }

    saveDeviation(deviationid) {
        this.dispatch();
        gallection.save(deviationid).then((data) => {
            if (data.success) {
                Toast.displayComponent(SaveToast, {added: true});
                this.actions.saveDeviationDone();
            } else {
                Toast.displayErrorMessage();
                this.actions.saveError();
            }
        }).catch((e)=>{
            if (e.error_description) {
                Toast.displayErrorMessage(e.error_description);
            } else {
                Toast.displayErrorMessage();
            }
            this.actions.saveError();
        });
        dapi.send();
    }

    unsaveDeviation(deviationid) {
        this.dispatch();
        gallection.unsave(deviationid).then((data) => {
            if (data.success) {
                Toast.displayComponent(SaveToast, {added: false, resave: this.actions.saveDeviation.bind(this, deviationid)});
                this.actions.unsaveDeviationDone();
            } else {
                Toast.displayErrorMessage();
                this.actions.saveError();
            }
        }).catch((e)=>{
            if (e.error_description) {
                Toast.displayErrorMessage(e.error_description);
            } else {
                Toast.displayErrorMessage();
            }
            this.actions.saveError();
        });
        dapi.send();
    }

    postComment(type, id, ownerid, commentBody, parentid) {
        this.dispatch();
        switch (type) {
            case 'collection':
                comments.postCollection(id, ownerid, commentBody, parentid).then((data) => {
                    this.actions.postCommentDone(data);
                });
                break;
            case 'status':
                comments.postStatus(id, ownerid, commentBody, parentid).then((data) => {
                    this.actions.postCommentDone(data);
                });
                break;
            case 'poll':
                comments.postPoll(id, ownerid, commentBody, parentid).then((data) => {
                    this.actions.postCommentDone(data);
                });
                break;
            default:
                if (DeviationTypes.test(type)) {
                    comments.postDeviation(id, ownerid, commentBody, parentid).then((data) => {
                        this.actions.postCommentDone(data);
                    });
                }
                break;
        }
    }

    postCommentDone(data) {
        this.dispatch(data);

        // Inform PeekWriter that the comment's complete
        DWait.ready('jms/lib/pubsub.js', () => {
            PubSub.publish('WriterAnywhere.shutdown');
        });
    }

    fetchCollectionGrid(PEEK_TORPEDO_NAME, deviationids) {
        deviation.torpedoHtml(PEEK_TORPEDO_NAME, deviationids).then((response) => {
            this.actions.fetchCollectionGridDone(response);
        });
    }

    vote(pollid, userid, answerid) {
        this.dispatch();
        poll.vote(pollid, userid, answerid).then((poll) => {
            this.actions.voteDone({poll: poll});
        }, () => {
            this.actions.voteDone(false);
        }).catch(() => {
            this.actions.voteDone(false);
        });
    }
}

module.exports = Actions;
