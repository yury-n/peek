'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

const Status = require('lib/da/peek/components/info/status');
const Collection = require('lib/da/peek/components/info/collection');
const Deviation = require('lib/da/peek/components/info/deviation');
const DeviationTypes = require('lib/da/peek/constants/deviation-types');
const Poll = require('lib/da/peek/components/info/poll');

class Info extends ReactBase {
    render() {
        let InfoComponent;
        switch (this.props.type) {
            case 'status':
                InfoComponent = Status;
                break;
            case 'collection':
                InfoComponent = Collection;
                break;
            case 'poll':
                InfoComponent = Poll;
                break;
            default:
                if (DeviationTypes.test(this.props.type)) {
                    InfoComponent = Deviation;
                }
                break;
        }

        if (InfoComponent) {
            return <InfoComponent {...this.props} />
        }
    }
}

module.exports = Info;
