'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');

class FaveNewCollection extends ReactBase {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // focus Name first
        ReactDOM.findDOMNode(this.refs.name).focus();
    }

    handleChange(e) {
        this.props.actions.faveNewCollectionChange({
            name: e.target.name,
            value: e.target.type == 'checkbox' ? e.target.checked : e.target.value
        });
    }

    handleHide() {
        this.props.hide();
        this.props.delete();
    }

    handleSubmit() {
        if (this.props.newCollection.name.length === 0) {
            return this.props.actions.faveShowError("Name is required");
        }

        this.props.actions.faveNewCollectionSubmit(
            this.props.newCollection.name,
            this.props.newCollection.description,
            this.props.newCollection.tags,
            this.props.newCollection.antisocial
        );

        this.props.hide();
        this.props.delete();
    }

    render() {
        return (
            <div className="fave-new-collection">
                <header>
                    <h3>Create a Collection</h3>
                    <div className="desc">Group your favorites together</div>
                    <a href="javascript:void(0)" onClick={this.handleHide} className="close"><i className="icon-close"></i></a>
                </header>
                <div className="content">
                    <form>
                        <section className="form-group">
                            <label className="">Title</label>
                            <input type="text" className="text" name="name" ref="name" onChange={this.handleChange} placeholder="'Digital Art' or 'Amazing Photography'" />
                        </section>
                        <section className="form-group">
                            <label>Description</label>
                            <textarea cols="50" rows="6" onChange={this.handleChange} placeholder="Talk about this collection" name="description"></textarea>
                        </section>
                        <section className="form-group">
                            <label>Tags</label>
                            <input type="text" className="text" name="tags" onChange={this.handleChange} placeholder="Enter some tags (displayed publicly)" />
                        </section>
                        <section className="form-group">
                            <label>Sharing</label>
                            <div className="checkbox">
                                <input type="checkbox" defaultChecked onChange={this.handleChange} name="antisocial" /> <span className="text">Allow watchers to receive updates on this collection</span>
                            </div>
                        </section>
                    </form>
                </div>
                <footer>
                    <a href="javascript:void(0)" onClick={ this.handleHide } className="btn default"><b>Cancel</b></a>
                    <a href="javascript:void(0)" onClick={ this.handleSubmit } className="btn primary"><b>Create</b></a>
                </footer>
            </div>
        );
    }
}

module.exports = FaveNewCollection;