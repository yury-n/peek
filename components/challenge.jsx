'use strict';

const { React, ReactBase } = require('lib/tp/react-base');

class ChallengeExtras extends ReactBase {
    render() {
        return (
            <div className="dds challenge">
                <div className="title">
                    <h2>SKETCH THIS CHALLENGE</h2>
                    <h1><a href={ "http://www.deviantart.com/challenge/" + this.props.data.challengeid }>{ this.props.data.challenge_title }</a></h1>
                </div>
                <div class="challenge-button">
                    <a className="challenge-button" 
                       href="http://www.sketchbook.com/sketchthis">
                        <span className="challenge-inner"></span>
                    </a>
                </div>
            </div>);
    }
}

module.exports = ChallengeExtras;
