'use strict';

const dapi = require('lib/da/dapi');
const watchCache = require('lib/da/cache/watch');

class NotePopupActions {
    constructor() {
        this.generateActions(
            'setToValue',
            'setMessageValue',
            'sendNoteDone',
            'setPollData'
            );
    }
    sendNote(to, subject, body) {
        this.dispatch();

        if (!Array.isArray(to)) {
            to = [to];
        }

        dapi.request('/notes/send', {
            to: to,
            subject: subject,
            body: body
        }).then((data) => {
            if (data.results && data.results.length && data.results[0].success) {
                // we received a success message so really that to user
                this.actions.sendNoteDone('Note sent successfully.');
            } else {
                // server didn't return error status but we didn't get the
                // expected `success` response ¯\_(ツ)_/¯ this probably won't
                // happen
                this.actions.sendNoteDone('Something probably failed. ¯\_(ツ)_/¯');

            }
        }, (error) => {
            if (error.error_description) {
                // server returned an error description so show that
                this.actions.sendNoteDone('Error: ' + error.error_description);
            } else {
                // no error description so display generic error
                this.actions.sendNoteDone('Error: Server returned an error.');
            }
        });
        dapi.send();
    }
    fetchWatchees() {
        // retrieve alphabetically sorted watchees for autocompletion
        watchCache.watchees().then((watchees) => {
            this.dispatch(watchees);
        });
    }
}

module.exports = NotePopupActions;
