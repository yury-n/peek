'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const UserIcon = require('lib/da/user/components/user-icon');
const DA = require('lib/da/deviantart-object');
const sigil = require('lib/dom/sigil');
const classnames = require('lib/tp/classnames/classnames');
const comments = require('lib/da/comments');
const dapx = require('lib/da/dapx');

class CommentBox extends ReactBase {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
            preview: false
        };
        this.writerActive = false;
        this.writerInstance = null;
        this.blurTimeout;

        DWait.ready(['jms/lib/pubsub.js'], () => {
            PubSub.subscribe([{
                eventname: 'PeekWriter.init',
                subscriber: this,
                callback: (event, data) => {
                    if (data == this.props.id && !this.writerActive) {
                        PubSub.unsubscribe([{eventname: 'PeekWriter.init', subscriber: this}]);

                        this.writerActive = true;
                        this.refocus();
                        this.setState({ expanded: true })

                        this.setupWriter();
                    }
                }
            },
            {
                eventname: 'PeekWriter.activate',
                subscriber: this,
                callback: (event, data) => {
                    if (data == this.props.id) {
                        this.setupWriter();
                        this.refocus();
                        this.setState({ expanded: true })
                    }
                }
            },
            {
                eventname: 'PeekWriter.shutdown',
                subscriber: this,
                callback: (event, data) => {
                    if (data == this.props.id) {
                        this.writerActive = false;
                        this.writerInstance = null;
                        this.onBlur();
                    }
                }
            },
            ])
        });
    }

    setupWriter() {
        const writer = this.getWriter();
        if (this.writerInstance != writer) {
            this.writerInstance = writer;
            this.writerInstance.onblur = this.onBlur;
            this.writerInstance.onfocus = this.onFocus;
        }
    }

    getWriter() {
        if (this.refs.writerInput) {
            return ReactDOM.findDOMNode(this.refs.writerInput).previousSibling;
        }
        return false;
    }
    
    previewToggle(e) {
        if (!this.state.preview) {
            this.previewComment(e);
        } else {
            this.unpreview();
        }
    }

    getCommentBody(resetText) {
        const textArea = ReactDOM.findDOMNode(this.refs.writerInput);

        if (textArea) {
            const body = textArea.value;
            
            if (resetText) {
                textArea.value = '';
            }

            if (body.length) {
                return body;
            }
        }

        return false;
    }

    previewComment() {
        const body = this.getCommentBody();

        if (!body) {
            this.unpreview();
            return;
        }
        this.setState({ expanded : false, preview : "<div class='comment'><div class='comment-content'>Loading...</div></div>" });

        let promise;
        switch (this.type) {
            case 'collection':
                promise = comments.previewCollection(this.props.itemid, body);
                break;
            case 'status':
                promise = comments.previewStatus(this.props.itemid, body);
                break;
            default:
                promise = comments.previewDeviation(this.props.itemid, body);
                break;
        }

        promise.then((data) => {
            if (this.state.preview) {
                this.setState({ preview : data.preview });
            }
        }).catch(() => {
            this.unpreview();
        });
    }

    unpreview() {
        this.setState({ preview : false, expanded: true });
        this.refocus();
    }

    refocus() {
        if (this.state.expanded && this.refs.writerInput) {
            this.getWriter().focus();
        }
    }

    postComment() {
        const body = this.getCommentBody(true);
        if (!body) {
            return;
        }

        this.setState({ expanded : false });
        this.props.postComment(this.props.type, this.props.itemid, this.props.ownerid, body, this.props.parentid);
    }

    handleAddButtonClick() {
        this.postComment();

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'add_comment_btn',
            section: 'left_column',
            component: 'comments',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    onFocus() {
        window.clearTimeout(this.blurTimeout);
        this.setState({ expanded : true });
    }

    onBlur() {
        if (this.writerActive && this.state.expanded) {
            this.blurTimeout = window.setTimeout(() => {
                this.setState({ expanded : false });
            }, 200);
        }
    }

    toggleActive() {
        this.props.toggleActive();
    }

    generateClose() {
        if (!this.props.parentid) {
            return false;
        }

        return <span className="close-button" onClick={ this.props.toggleActive }></span>
    }

    generatePreview() {
        if (!this.state.preview) {
            return false;
        }

        return (<div className="preview" dangerouslySetInnerHTML={{__html: this.state.preview }} />);
    }

    generateTextArea() {
        let placeholder = 'Add A Comment';
        let key = 1;
        if (this.props.isCommenting) {
            placeholder = 'Saving...';
            // ok, this is a super hack that needs to go away.
            // the reason is because we need writer to fuck off, but it won't
            // unless we render a completely different textarea
            key = 0;
        }

        return (<textarea
                    ref="writerInput"
                    className={ classnames(['writeranywhere', {'active' : this.state.expanded}]) } 
                    onFocus={ this.onFocus }
                    onBlur={ this.onBlur }
                    onClick={ this.onClick }
                    disabled={ this.props.isCommenting }
                    placeholder={ placeholder }
                    data-type="peek"
                    data-id={ this.props.id }
                    key={ key }
                />);
    }

    render() {
        if (!this.props.isVisible) {
            return false;
        }

        return (<div className={ classnames(['comment-input', 'active', { 
                    'preview': this.state.preview,
                    'peek-comment-collapsed' : !this.state.expanded,
                    'peek-comment-expanded' : this.state.expanded
                }]) }>
                    <span className="avatar">
                        <UserIcon username={ DA.username } usericon={ DA.get('usericon') } />
                    </span>

                    { this.generateClose() }
                    { this.generatePreview() }
                    { this.generateTextArea() }

                    <span className={ classnames(['previewToggle', { 'edit' : this.state.preview }]) } onClick={ this.previewToggle }>
                        { this.state.preview ? 'EDIT' : 'PREVIEW' }
                    </span>
                    <button className="mega btn good" onClick={ this.handleAddButtonClick }>ADD A COMMENT</button>
                </div>);
    }
}

module.exports = CommentBox;
