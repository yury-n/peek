'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const classnames = require('lib/tp/classnames/classnames');
const dapx = require('lib/da/dapx');

class PinterestIcon extends ReactBase {
    handleClick() {
        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'pinterest',
            section: 'right_column',
            component: 'share_options',
            action: 'click'
        }, this.props.getDapxExtraData());
    }
    render() {
        let disabled = true;
        let url = 'javascript:void(0)';

        if (this.props.url) {
            disabled = false;
            url = 'http://pinterest.com/pin/create/button/?';
            url += 'url=' + encodeURIComponent(this.props.url);

            if (this.props.src) {
                url += '&media=' + encodeURIComponent(this.props.src);

                const description = this.props.title + ' by ' + this.props.author.username + '.deviantart.com on @DeviantArt';
                url += '&description=' + encodeURIComponent(description);
            } else {
                url += '&description=' + encodeURIComponent(this.props.description);
            }
        }

        return (
            <div className="share-icon-wrap">
                <a onClick={this.handleClick} href={url} target="_blank" className={classnames([
                        'share-icon',
                        'share-pinterest',
                        {'disabled': disabled}
                    ])}>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="300px" height="300px" viewBox="0 0 300 300" enable-background="new 0 0 300 300">
                        <g>
                            <path fill="#fff" d="M120.283,236.322c4.029-6.378,8.436-14.559,10.687-22.746c1.306-4.724,7.47-29.188,7.47-29.188
                c3.694,7.038,14.477,13.235,25.94,13.235c34.135,0,57.291-31.118,57.291-72.771c0-31.489-26.681-60.824-67.219-60.824
                c-50.448,0-75.887,36.167-75.887,66.329c0,18.263,6.91,34.509,21.745,40.565c2.431,0.993,4.609,0.032,5.316-2.657
                c0.484-1.864,1.654-6.567,2.163-8.521c0.711-2.664,0.438-3.598-1.526-5.918c-4.272-5.051-7-11.575-7-20.826
                c0-26.83,20.066-50.849,52.274-50.849c28.508,0,44.182,17.418,44.182,40.69c0,30.614-13.549,56.452-33.667,56.452
                c-11.112,0-19.419-9.187-16.759-20.45c3.185-13.45,9.377-27.972,9.377-37.682c0-8.687-4.666-15.941-14.324-15.941
                c-11.358,0-20.478,11.752-20.478,27.486c0,10.021,3.392,16.8,3.392,16.8s-11.621,49.237-13.662,57.857
                c-1.902,8.067-2.147,16.967-1.848,24.382C107.75,231.746,112.944,234.601,120.283,236.322z"></path>
                        </g>
                    </svg>
                </a>
            </div>
            );
    }
}

module.exports = PinterestIcon;
