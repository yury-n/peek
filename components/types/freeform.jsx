'ssets/src/js/lib/da/peek/components/types/freeform.jsxuse strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const sigil = require('lib/dom/sigil');
const deviation = require('lib/da/cache/deviation');
const classnames = require('lib/tp/classnames/classnames');

class Freeform extends ReactBase {
    componentDidUpdate() {
        if (this.props.contentLoaded && !this.dealtWithThings) {
            this.dealtWithThings = true;
            const embeds = ReactDOM.findDOMNode(this.refs.litContainer).querySelectorAll('iframe.daembed');
            for (let i in embeds) {
                let embed = embeds.item(i);
                if (embed && !sigil.has(embed, 'embedded-youtube')) {
                    sigil.add(embed, 'embedded-youtube');
                    let src = embed.getAttribute('src');
                    if (src.indexOf('enablejsapi') < 0) {
                        if (src.indexOf('?') > 0) {
                            src += '&enablejsapi=1';
                        } else {
                            src += '?enablejsapi=1';
                        }
                        embed.setAttribute('src', src);
                    }
                }
            }
            
            /***
             * Scripts embeded in freeform don't execute when they're inserted. This is a Bad Thing.
             */
            const scripts = ReactDOM.findDOMNode(this.refs.litContainer).getElementsByTagName('script');

            for (let i = 0; i < scripts.length; i++) {
                let script = scripts.item(i);
                let replacement = document.createElement('script');
                replacement.text = script.innerHTML;
                for (let i = script.attributes.length-1; i >= 0; i--) {
                    replacement.setAttribute( script.attributes[i].name, script.attributes[i].value);
                }
                script.parentNode.replaceChild(replacement, script);
            }
        }
    }
    render() {
        if (!this.props.contentLoaded) {
            return null;
        }

        let html = this.props.freeformContent.html;
        if (this.props.freeformContent.css) {
            html = '<style>' + this.props.freeformContent.css + '</style>' + html;
        }

        return (
            <div className="lit-container">
                <div 
                    className={ classnames(['deviation-text', 'scrollable'], {'no-skin': !this.props.freeformContent.css }) } 
                    ref="litContainer"
                    dangerouslySetInnerHTML={{__html: html}}
                    data-sigil="peek-scrollable-content" 
                />
                <div className="clear"></div>
            </div>
        );
    }

}

module.exports = Freeform;
