'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Loading = require('lib/da/peek/components/loading');
// const deviations = require('lib/da/api/deviation');

class Flash extends ReactBase {
    getSrc() {
        if (this.props.position === 0 && this.props.visible) {
            let url = this.props.nodeMetadata.embed || '';
            return url.replace('http:', '');
        }
        return '';
    }
    render() {
        
        return (
            <div className="deviation-flash">
                <iframe className="flashtime" src={ this.getSrc() } 
                        height={ this.props.nodeMetadata.embedHeight } 
                        width={ this.props.nodeMetadata.embedWidth }
                        frameBorder="0" scrolling="no" noresize="noresize"></iframe>
            </div>
        )
    }
}

module.exports = Flash;
