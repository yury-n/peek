'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Loading = require('lib/da/peek/components/loading');
// const deviations = require('lib/da/api/deviation');

class Madefire extends ReactBase { 
    render() {
        let css = {
            height : this.props.nodeMetadata.embedHeight,
            width : this.props.nodeMetadata.embedWidth
        }
        return (
            <div className="deviation-madefire">
                <iframe  
                    style={ css }
                    src={ this.props.nodeMetadata.embedurl }
                    frameBorder="0">
                </iframe>
            </div>)
    }
}

module.exports = Madefire;
