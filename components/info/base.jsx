'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const { ReactDOM } = require('lib/tp/react-dom');
const dateFormatter = require('lib/da/date-formatter');
const sigil = require('lib/dom/sigil');
const Torpedo = require('lib/da/torpedo/torpedo');
const classnames = require('lib/tp/classnames/classnames');
const DA = require('lib/da/deviantart-object');
const WhoButton = require('lib/da/peek/components/who');
const Report = require('lib/da/peek/components/report');
const Share = require('lib/da/peek/components/share');
const UserLink = require('lib/da/user/components/user-link');
const UserIcon = require('lib/da/user/components/user-icon');
const ShareButton = require('lib/da/peek/components/share-button');
const TabLinksContainer = require('lib/da/peek/components/tab-links-container');
const CopyInput = require('lib/da/peek/components/copy-input');
const dapx = require('lib/da/dapx');
const listener = require('lib/dom/event-listener');

const torpedoOptions = {
    showOrphans: false,
    insertBands: false,
    maxRows : 2,
    targetThumbHeight: 150,
    thumbPadding : 12
};

class InfoBase extends ReactBase {
    constructor(props) {
        super(props);

        this.inittedByContainerRef = {};
        this.peeksByContainerRef = {};
        this.relatedArtRequested = false;

        // we lose our automatic bind this with all the extending
        this._logMltThumbClick = this.logMltThumbClick.bind(this);

        this.state = {
            isDescriptionCollapsible: false
        };
    }
    
    handleWatchClick() {
        if (!this.props.author.is_watching) {
            this.props.actions.watchUser(this.props.author.username);
        } else {
            this.props.actions.unwatchUser(this.props.author.username);
        }

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'watch_btn',
            section: 'right_column',
            component: 'info_tab',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    handleTabClick(tabname) {
        // trigger fetching of content for Related Art tab
        if (!this.relatedArtRequested) {
            this.relatedArtRequested = true;
            this.props.actions.fetchRelatedArt(this.props.type, this.props.author.username, this.props.id);
        }
        this.props.actions.setActiveInfoTab(tabname);

        // init torpedo in case fetchRelatedArt() returned with data but tab was hidden
        // which prevents torpedo from booting up due to container width being unset by DOM
        if (tabname == 'related art') {
            this.updateTorpedos();
        }
    }

    updateTorpedos() {
        window.requestAnimationFrame(() => {
            this.activateTorpedo('related-art-artist-container', 'mfa');
            this.activateTorpedo('related-art-da-container', 'mlt');
        });
    }

    handleDescriptionCollapserClick() {
        this.props.actions.toggleInfoDescriptionCollapsed();
    }

    componentDidMount() {
        this.descriptionNode = ReactDOM.findDOMNode(this.refs['description-content']);
        this.determineDescriptionState();

        const relatedArtContainer = ReactDOM.findDOMNode(this.refs.relatedArtContainer);
        listener.on('click', 'torpedo-thumb', this._logMltThumbClick, false, relatedArtContainer);
    }

    componentWillUnmount() {
        listener.off('click', 'torpedo-thumb', this._logMltThumbClick);

        // delete nested peeks
        const keys = Object.keys(this.peeksByContainerRef);
        for (let i = 0; i < keys.length; i++) {
            this.peeksByContainerRef[keys[i]].delete();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        this.determineDescriptionState();
        if (this.props.activeTab == 'related art') {
            this.updateTorpedos();
        }
    }

    determineDescriptionState() {
        if (!this.state.isDescriptionCollapsible && this.descriptionNode) {
            const descriptionStyle = window.getComputedStyle(this.descriptionNode);
            const maxHeight = descriptionStyle.getPropertyValue('max-height');

            // This'll need to be kept in sync with any changes to info.less
            if (
                (maxHeight == '88px' && this.descriptionNode.offsetHeight > 80) ||
                (maxHeight == '176px' && this.descriptionNode.offsetHeight > 168)
            ) {
                // need to set state so that we force the component to re-render
                this.setState({
                    isDescriptionCollapsible: true
                });
            }
        }
    }

    logMltThumbClick() {
        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'mlt_thumb',
            section: 'right_column',
            component: 'related_art',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    activateTorpedo(containerRef, context) {
        if (this.inittedByContainerRef[containerRef]) {
            return;
        }

        const containerNode = ReactDOM.findDOMNode(this.refs[containerRef]);
        // check if node is ready for torpedo sizing
        if (containerNode.offsetWidth === 0 || !sigil.first(containerNode, 'peekable')) {
            return;
        }
        this.inittedByContainerRef[containerRef] = true;

        const t = new Torpedo(containerNode, torpedoOptions);
        t.generate();
        // nothing about this torpedo will change after first generate so
        // deactivate immediately
        t.deactivate();

        window.requestAnimationFrame(() => {
            const slideProps = {
                isNested: true,
                context: context
            };
            this.peeksByContainerRef[containerRef] = this.props.peek(containerNode, 'peekable', slideProps);
            this.peeksByContainerRef[containerRef].updateNodes();
        });
    }

    submitted() {
        // gotta use milliseconds
        return dateFormatter.parse(this.props.publishedTime);
    }

    generateAd() {
        // no ad for now
        return <div />
        // <img src="//st.deviantart.net/watchfeed/placeholder-for-adv.png" />
        // <a href="#" className="upsell">CORE MEMBERS DO NOT SEE ADS. LEARN MORE</a>
    }

    generateAfterDescription() {
        return false;
    }

    generateFaveButton() {
        return false;
    }

    generateFaveDropdown() {
        return false;
    }

    generateDownloadButton() {
        return false;
    }

    generateWatchButton() {
        let watchButton = false;

        // for nested peeks (Specifically from collections)
        // we want to allow watching the user
        if (this.props.isNested) {
            let watchContent = 'Watch';
            if (this.props.author.is_watching) {
                watchContent = <span className="slidable"><span className="segment">watching</span><span className="segment">unwatch</span></span>;
            }
            watchButton = <span className={ classnames(['btn', 'tiny', 'watch', { 'clear-good' : !this.props.author.is_watching, 'clear-bad' : this.props.author.is_watching}])} onClick={ () => { this.handleWatchClick() }}>{ watchContent }</span>
        }
        
        return watchButton;
    }

    generateSaveButton() {
        return false;
    }

    handleShareButtonClick() {
        this.props.actions.toggleShareOpen();

        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'share_btn',
            section: 'right_column',
            component: 'buttons_block',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    generateShareButton() {
        return <ShareButton onClick={ this.handleShareButtonClick.bind(this) } isOpen={ this.props.shareOpen } />;
    }

    /**
     * Override in child classes with the value to be applied for the `left`
     * style of the triangle extending out of the top of the share container
     * box. Triangle should point to the share button, the location of which
     * can vary based on the button visible for this particular slide.
     *
     * @return {String} The value for the `left` style (e.g. "205px")
     */
    getTriangleLeft() {
        return false;
    }

    // override in child class
    generateShare() {
        return false;
    }

    generateTabLinks() {
        return (
            <TabLinksContainer
                tabs={ ['info', 'stats', 'related art'] }
                changeActiveTab={ this.handleTabClick.bind(this) }
                activeTab={ this.props.activeTab } />
            );
    }

    handleTitleClick() {
        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'dev_title',
            section: 'right_column',
            component: 'info_tab',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    generateInfo() {
        return (<div>
                    <div className="artist-info">
                        { this.generateUsericon() }
                        { this.generateUserlink() }
                        { this.generateWatchButton() }
                    </div>

                    <h2 className="deviation-title">
                        <a onClick={ this.handleTitleClick.bind(this) } href={ this.props.url } target="_blank">
                            { this.props.title }
                        </a>
                    </h2>

                    { this.generateInfoContent() }
                </div>);
    }

    generateUsericon() {
        return <UserIcon className="artist-icon" usericon={ this.props.author.usericon } username={ this.props.author.username } />;
    }

    generateUserlink() {
        return <UserLink className="artist-name" username={ this.props.author.username } />;
    }

    generateCopyright() {
        const submitted = this.submitted();

        return <span className="copyright" dangerouslySetInnerHTML={{__html: "&copy; " + submitted.getFullYear() + " " + this.props.author.username + '. ' + 'All rights reserved.' }} />;
    }

    generateInfoContent() {
        return (<div className="info">
                    <div className="stats">
                        { this.generateInfoViews() }
                        { this.generateInfoComments() }
                        { this.generateInfoFaves() }
                    </div>

                    { this.generateDescription() }
                    { this.generateAfterDescription() }

                    <div className="attributions">
                        { this.generateSubmitted() }
                        { this.generateCopyright() }
                        { this.generateReportButton() }
                    </div>
                </div>);
    }

    generateInfoViews() {
        return (<span className="stat stat-views" title="Views">
                    <i className="icon-eye-fill-simple"></i>
                    <b>{ this.formatNumber(this.props.stats.views) }</b>
                </span>);
    }

    generateInfoComments() {
        return (<a href="javascript:void(0)" onClick={this.props.expandComments} className="stat stat-comments" title="Comments">
                    <i className="icon-speech-fill"></i>
                    <b>{ this.formatNumber(this.props.stats.comments) }</b>
                </a>);
    }

    generateInfoFaves() {
        return (<span className="stat stat-faves" title="Faves">
                    <i className="icon-fave-fill"></i>
                    <b>{ this.formatNumber(this.props.stats.favourites) }</b>
                </span>);
    }

    generateDescription() {
        let descriptionToggler;
        if (this.state.isDescriptionCollapsible) {
            descriptionToggler = (<div className="collapser" onClick={ this.handleDescriptionCollapserClick.bind(this) }>{ this.props.descriptionCollapsed ? 'MORE' : 'LESS' }</div>);
        }

        return (<div 
                    ref="description-content"
                    className={ classnames('description', {
                                    collapsed: this.props.descriptionCollapsed,
                                    expanded: !this.props.descriptionCollapsed
                                }) }
                >
                    <p dangerouslySetInnerHTML={{__html: this.props.truncatedDescription }} />
                    { descriptionToggler }
                </div>);
    }

    generateSubmitted() {
        const submitted = this.submitted();

        return (<div className="submitted" title={dateFormatter.mdy(submitted) + ' @ ' + dateFormatter.giA(submitted)}>Submitted on {dateFormatter.mdy(submitted)}</div>);
    }

    generateReportButton() {
        return <Report data={ {id : this.props.id} } />;
    }

    generateStats(commentCount) {
        const submitted = this.submitted();
        if (commentCount === undefined) {
            commentCount = this.formatNumber(this.props.stats.comments);
        }

        return (<div>
                    <table className="data">
                        <tbody>
                            <tr><th>Submitted</th>          <td>{ this.props.publishedTime ? dateFormatter.mdy(submitted) : 'N/A' }</td></tr>
                            <tr><th>Image Size</th>         <td>{ this.props.stats.size }</td></tr>
                            <tr><th>Views</th>              <td>{ this.formatNumber(this.props.stats.views) } <em>({ this.formatNumber(this.props.stats.views_today) } today)</em></td></tr>
                            <tr><th>Downloads</th>          <td>{ this.formatNumber(this.props.stats.downloads) } <em>({ this.formatNumber(this.props.stats.downloads_today) } today)</em></td></tr>
                            <tr><th>Favourites</th>         <td>{ this.formatNumber(this.props.stats.favourites) } (<WhoButton id={ this.props.id } title={this.props.title } />)</td></tr>
                            <tr><th>Comments</th>           <td>{ commentCount }</td></tr>
                        </tbody>
                    </table>

                    <CopyInput label="Link" value={ this.props.id ? 'http://fav.me/d' + (new Number(this.props.id)).toString(36) : '' } />
                    <CopyInput label="Thumb" value={ this.props.id ? ':thumb' + this.props.id + ':' : '' } />
                </div>);
    }

    handleRelatedArtTitleClick() {
        // log event T26830
        dapx.sendStandardizedPayload({
            view: 'peek',
            element: 'mlt_title',
            section: 'right_column',
            component: 'related_art',
            action: 'click'
        }, this.props.getDapxExtraData());
    }

    generateRelatedArt() {
        return (<div ref="relatedArtContainer">
                    <h2>
                        <a onClick={ this.handleRelatedArtTitleClick.bind(this) } href={ 'http://' + this.props.author.username + '.deviantart.com'} target="_blank">
                            More from <strong>{this.props.author.username}</strong> <i className="icon-arrow"></i>
                        </a>
                    </h2>
                    <div className="related-art-artist torpedo-container" ref="related-art-artist-container" dangerouslySetInnerHTML={{__html: this.props.relatedArt.mfa }}></div>
                    <h2>
                        <a onClick={ this.handleRelatedArtTitleClick.bind(this) } href={ 'http://browse.deviantart.com/morelikethis/' + this.props.id } target="_blank">
                            More from <strong>DeviantArt</strong> <i className="icon-arrow"></i>
                        </a>
                    </h2>
                    <div className="torpedo-container" ref="related-art-da-container" dangerouslySetInnerHTML={{__html: this.props.relatedArt.mlt }}></div>
                </div>);
    }

    formatNumber(x) {
        return x !== undefined ? new String(x).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '';
    }

    isActive(tab) {
        return this.props.activeTab === tab
    }

    render() {

        return (
            <div className="peek-info-component">
                <div className="deviation-info">
                
                    { this.generateAd() }

                    <div className="button-block">
                        { this.generateFaveButton() }
                        { this.generateSaveButton() }
                        { this.generateDownloadButton() }
                        { this.generateShareButton() }
                    </div>

                    { this.generateFaveDropdown() }
                    { this.generateShare() }
                    { this.generateTabLinks() }

                    <div ref="tab-container" className="tab-container">
                        <div ref="tab-info" className={ classnames('tab-content', {active: this.isActive('info')}) }>
                            { this.generateInfo() }
                        </div>

                        <div ref="tab-stats" className={ classnames('tab-content', {active: this.isActive('stats')}) }>
                            { this.generateStats() }
                        </div>

                        <div ref="tab-related-art" className={ classnames('tab-content', {active: this.isActive('related art')}) }>
                            { this.generateRelatedArt() }
                        </div>
                    </div>
                </div>
            </div>
       );
    }
}

module.exports = InfoBase;
