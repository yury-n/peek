'use strict';

const { React, ReactBase } = require('lib/tp/react-base');
const Loading = require('lib/da/peek/components/loading');
const UserIcon = require('lib/da/user/components/user-icon');
const UserLink = require('lib/da/user/components/user-link');
const dateFormatter = require('lib/da/date-formatter');
const classnames = require('lib/tp/classnames/classnames');

class StatusBody extends ReactBase {
    getShareText() {
        if (this.props.isShare) {
            if (this.props.shareType == 'deviation') {
                return 'shared a deviation';
            } else {
                return 'shared a status';
            }
        }

        return 'posted a status';
    }
    getAttachment() {
        if (this.props.isShare) {
            if (this.props.attachment) {
                return (<div className="torpedo-container show-orphans">
                            {this.props.attachment}
                        </div>);
            } else {
                return (<Loading text="Loading Share..." classes="attachment" />);
            }
        }

        return false;
    }
    render() {
        if (this.props.loading) {
            return (<Loading 
                        text="Loading Status..." 
                        classes="status" />);
        }

        const classes = classnames(['status', 'no-skin'], {
            'with-attachment': this.props.isShare
        });

        if (
            (this.props.isShare && this.props.shareType === 'error')
            || !this.props.author
            || !this.props.author.username
        ) {
            const className = (this.props.attachment) ? 'attachment' : 'status';
            return (<Loading text="Error" classes={className} llamaless={true} />);
        }

        return (
            <div className={classes}>
                <div className="status-top">
                    <UserIcon className="status-usericon" username={this.props.author.username} usericon={this.props.author.usericon} />
                    <UserLink className="status-userlink" username={this.props.author.username} />
                    <div className="status-posted">{this.getShareText()}</div>

                    <div className="status-date">
                        {dateFormatter.mdy(dateFormatter.parse(this.props.ts))}
                    </div>
                </div>
                <div className="status-body" dangerouslySetInnerHTML={{__html: this.props.body }}></div>
                {this.getAttachment()}
            </div>
        );
    }
}

class Status extends ReactBase {
    isShare() {
        return this.props.status.is_share;
    }
    getShareType() {
        if (this.isShare()) {
            // if we know we're a share, we're guaranteed to have this.props.content.items
            // as it's in the return object attached to this.props.content
            return this.props.status.items[0].type;
        }

        // something went wrong... 
        // we shouldn't use this if it's not a share anyway, but just in case
        return 'error';
    }
    getAttachment() {
        if (!this.isShare()) {
            return false;
        }

        if (this.props.status.attachment) {
            switch (this.props.status.attachment.type) {
                case 'status':
                    const status = this.props.status.attachment.status;
                    return (<StatusBody 
                                    body={status.body}
                                    author={status.author}
                                    ts={status.ts}
                                />);
                    break;
                case 'error':
                    return (<Loading text="Share Not Found" classes="attachment" llamaless={true} />);
                    break;
                default:
                    return (<div dangerouslySetInnerHTML={{__html: this.props.status.attachment.html}}></div>);
            }
        } else {
            return (<Loading text="Loading Share..." classes="attachment" />);
        }
    }
    render() {
        if (!this.props.contentLoaded) {
            return null;
        }

        return (<StatusBody 
                    loading={false} 
                    attachment={this.getAttachment()} 
                    isShare={this.isShare()} 
                    shareType={this.getShareType()}
                    body={this.props.status.body}
                    author={this.props.status.author}
                    ts={this.props.status.ts}
                />);
    }
}

module.exports = Status;

