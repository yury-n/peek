'use strict';

const React = require('lib/tp/react');
const InfoBase = require('lib/da/peek/components/info/base');
const FaveButton = require('lib/da/peek/components/fave-button');
const SaveButton = require('lib/da/peek/components/save-button');
const DownloadButton = require('lib/da/peek/components/download-button');
const FaveDropdown = require('lib/da/peek/components/fave-dropdown');
const Category = require('lib/da/peek/components/category');
const Tags = require('lib/da/peek/components/tags');
const Share = require('lib/da/peek/components/share');
const DA = require('lib/da/deviantart-object');

class InfoDeviation extends InfoBase {
    handleFaveClick() {
        if (!this.props.isFaving) {
            this.props.actions.faveClick(this.props.isFavourited, this.props.id, DA.username, this.props.isSaved);
        }
    }

    // handle internally so we can pass on the selected collectionid and icon
    handleFaveDropdownSelect(folderid, collectionTitle) {
        this.props.actions.faveDropdownSelect({
            id: this.props.id,
            title: this.props.title,
            folderid: folderid,
            collectionTitle : collectionTitle,
            icon: (this.props.nodeMetadata.sizing ? this.props.nodeMetadata.sizing[0].src : null)
        });
    }

    // handle internally so we can pass on the collectionsLoaded property
    handleFaveDropdownClick() {
        this.props.actions.faveDropdownClick(this.props.collectionsLoaded);
    }

    generateFaveButton() {
        return <FaveButton 
                    isFavourited={ this.props.isFavourited }
                    isFaving={ this.props.isFaving }
                    faveClick={ this.handleFaveClick }
                    isFaveDropdownActive={ this.props.isFaveDropdownActive }
                    faveDropdownClick={ this.handleFaveDropdownClick }
                    position={ this.props.position }
                    getDapxExtraData={ this.props.getDapxExtraData }
                />;
    }

    generateFaveDropdown() {
        return <FaveDropdown
                    collections={ this.props.collections }
                    collectionsLoaded={ this.props.collectionsLoaded }
                    isFaveDropdownActive={ this.props.isFaveDropdownActive }
                    faveDropdownSelect={ this.handleFaveDropdownSelect }
                    actions={this.props.actions}
                    newCollection={ this.props.newCollection }
                    visible={ this.props.visible }
                    getDapxExtraData={ this.props.getDapxExtraData }
                />;
    }

    generateSaveButton() {
        if (this.props.hideSaveButton) {
            return false;
        }
        return (
            <SaveButton
                deviationid={ this.props.id }
                disabled={ this.props.isFavourited || this.props.isFaving || this.props.isSaving }
                isSaved={ this.props.isSaved }
                saveDeviation={ this.props.actions.saveDeviation }
                position={ this.props.position }
                unsaveDeviation={ this.props.actions.unsaveDeviation } />
            );
    }

    generateDownloadButton() {
        if (this.props.downloadUrl.length == 0) {
            return false;
        }

        return (<DownloadButton
            downloadUrl={ this.props.downloadUrl }
            position={ this.props.position }
        />);
    }

    generateShare() {
        return (<Share
            id={ this.props.nodeMetadata.id }
            author={ this.props.author }
            isOpen={ this.props.shareOpen }
            type={ this.props.type }
            sizing={ this.props.nodeMetadata.sizing }
            url={ this.props.url }
            title={ this.props.title }
            mature={ this.props.nodeMetadata.mature }
            description={ this.props.truncatedDescription }
            triangleLeft={ this.getTriangleLeft() }
            getDapxExtraData={ this.props.getDapxExtraData }
        />);
    }

    generateAfterDescription() {
        if (!this.props.contentLoaded) {
            return false;
        }
        
        return (<div>
                    <Category categoryPath={ this.props.categoryPath } categoryPathTitles={ this.props.categoryPathTitles } />
                    <Tags actions={ this.props.actions } tags={ this.props.tags } tagsCollapsed={ this.props.tagsCollapsed } />
                </div>);
    }

    generateInfoComments() {
        if (this.props.allowsComments !== false) {
            return super.generateInfoComments();
        }

        return false;
    }

    generateStats() {
        return super.generateStats((this.props.allowsComments === false) ? 'Disabled' : this.formatNumber(this.props.stats.comments));
    }

    getTriangleLeft() {
        return this.props.hideSaveButton ? '158px' : '205px';
    }
}

module.exports = InfoDeviation;
